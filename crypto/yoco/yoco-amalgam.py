#!/usr/local/bin/python

import itertools
import os

from flag import FLAG

# truncates output to the shorter of the lengths of its arguments
def truncating_xor(a, b):
    return bytes(ai ^ bi for ai, bi in zip(a, b))

def mix_otps(a, b, c):
    return bytes(itertools.chain.from_iterable(zip(a, b, c)))

# no wrapping attacks this time!
OTP_LEN = 48
assert len(FLAG) < OTP_LEN

# generate 3 random OTPs
otps = [os.urandom(48) for _ in range(3)]

# encrypt flag with proprietary Entropy Mix™ algorithm
flag_otp = mix_otps(*otps)
encrypted_flag = truncating_xor(FLAG, flag_otp)

print("Okay fine we've learned our lesson, we're not gonna reuse one-time pads anymore. We've gone ahead and invented the Entropy Mix™ algorithm (patent pending) to maintain utmost security and efficiency.")
print("Since we're always looking for new customers, we're giving you a free trial of it so you can witness its awesomeness :)")

for otp in otps:
    msg = input("Please a message to encrypt: ").encode()
    ciphertext = truncating_xor(msg, otp)
    print(f"Your ciphertext (in hex) is: {ciphertext.hex()}")
    print()

print("And finally the encrypted flag, of course. Not that you can decrypt it this time since we didn't reuse any one-time pads :)")
print(encrypted_flag.hex())
