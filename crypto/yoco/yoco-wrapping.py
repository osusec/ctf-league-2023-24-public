#!/usr/local/bin/python

import itertools
import os

from flag import FLAG

# wraps shorter of two arguments during xor process
def wrapping_xor(a, b):
    if len(a) <= len(b):
        a_iter = itertools.cycle(a)

        return bytes(ai ^ bi for ai, bi in zip(a_iter, b))
    else:
        b_iter = itertools.cycle(b)
        return bytes(ai ^ bi for ai, bi in zip(a, b_iter))

otp = os.urandom(4)
encrypted_flag = wrapping_xor(FLAG, otp)

print("This time you don't get to encrypt anything, you just get the encrypted flag. There's no way you can decrypt it since I didn't give you the key, right?")
print(encrypted_flag.hex())
