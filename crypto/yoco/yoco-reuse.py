#!/usr/local/bin/python

import os

from flag import FLAG

# truncates output to the shorter of the lengths of its arguments
def truncating_xor(a, b):
    return bytes(a ^ b for a, b in zip(a, b))

otp = os.urandom(len(FLAG))
encrypted_flag = truncating_xor(FLAG, otp)

print(f"The encrypted flag is {encrypted_flag.hex()}, I bet you can't decrypt it!")
print("I'll let you encrypt something else with my key though since I'm feeling lucky :)")

# convert user input to bytes using .encode() since otherwise truncating_xor won't work properly
user_input = input("Please enter your message: ").encode()

enc_input = truncating_xor(user_input, otp)

print(f"Here's your encrypted message: {enc_input.hex()}")
print("Hope you enjoy not having the flag! Muahahaha")
