from pwn import *

# io = remote("chal.ctf-league.osusec.org", 1306)
io = remote("localhost", 1306)

io.recvuntil(b"is ")

flag_hex = io.recvuntil(b",", drop=True).decode()
flag_enc = bytes.fromhex(flag_hex)

null_msg = b"\x00"*len(flag_enc)

io.sendline(null_msg)

io.recvuntil(b"encrypted message: ")

otp_hex = io.recvline().decode()
otp = bytes.fromhex(otp_hex)

print("Flag:", xor(otp, flag_enc).decode())
