import itertools

from pwn import *

def mix_otps(a, b, c):
    return bytes(itertools.chain.from_iterable(zip(a, b, c)))

# io = remote("chal.ctf-league.osusec.org", 1308)
io = remote("localhost", 1308)

OTP_LEN = 48

null_msg = b"\x00"*OTP_LEN
otps = []

for _ in range(3):
    io.sendlineafter(b"encrypt: ", null_msg)
    io.recvuntil(b"is: ")
    otp_hex = io.recvline().decode()
    otps.append(bytes.fromhex(otp_hex))

io.recvuntil(b":)\n")
flag_hex = io.recvline().decode()
flag_enc = bytes.fromhex(flag_hex)

flag_otp = mix_otps(*otps)
flag_dec = xor(flag_otp, flag_enc, cut="min").decode()
print("Flag:", flag_dec)
