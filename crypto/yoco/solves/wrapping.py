from pwn import *

# io = remote("chal.ctf-league.osusec.org", 1307)
io = remote("localhost", 1307)

io.recvline()
flag_hex = io.recvline().decode()
flag_enc = bytes.fromhex(flag_hex)

otp = xor(flag_enc, b"osu{", cut="min")

flag = xor(otp, flag_enc)
print("Flag:", flag.decode())
