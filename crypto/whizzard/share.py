#!/usr/local/bin/python3

import os
import secrets

def eval_poly(coefficients, x):
    """Evaluates a polynomial, whose coefficients are listed in order of increasing degree"""
    result = 0
    for coeff in reversed(coefficients):
        result = coeff + x*result
    return result

def share_poly(value, threshold):
    coefficients = [value] + [secrets.randbits(512) for _ in range(threshold - 1)]
    return coefficients

THRESHOLD = 3
MAX_SHARE = pow(2, 256)

# (second argument is not a real flag)
flag_str = os.getenv("FLAG", "osu{local-flag-for-testing}")
assert len(flag_str) < 64

flag = int.from_bytes(flag_str.encode(), byteorder="big")
flag_poly = share_poly(flag, THRESHOLD)

print("Shamir secret sharing is super cool!! I can tell people stuff and they'll have no idea what I'm saying unless they collaborate but who would do that?")
print("In fact I trust its security so much that I'll give you two shares of one of my secrets! Not that you'll be able to learn it though cause you'll need three shares to do that :^)")

for _ in range(2):
    try:
        share = int(input("Which share would you like? "))
    except ValueError:
        print("That's not a number silly")
        continue

    if share < 1 or share >= MAX_SHARE:
        print("I don't like your number so I'm not giving that to you")
    else:
        print("Here's your share:", eval_poly(flag_poly, share))
