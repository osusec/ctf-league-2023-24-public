#!/usr/local/bin/python3

import os
import secrets
import sys

DEGREE = 7
p = 327480268692574562614176616743456356641

def eval_poly(coefficients, x):
    """Evaluates a polynomial, whose coefficients are listed in order of increasing degree"""
    result = 0
    for coeff in reversed(coefficients):
        result = (coeff + x*result) % p
    return result

print("I've heard rumors that you're super good at math, is it true??")
print("To prove it I want you to evaluate 50 polynomials for me, sound good?")
print(f"Oh and this is all done mod {p} cause I wouldn't want to deal with *decimals*")

for _ in range(50):
    # generate random polynomial & provide coefficients
    coefficients = [secrets.randbelow(p) for _ in range(DEGREE + 1)]
    print("Here are your coefficients:")
    print(coefficients)

    # choose random x value to evaluate at
    x_val = secrets.randbelow(p)
    print("Now tell me what the y-value at this point is:")
    print(x_val)

    try:
        guess = int(input("y-value: ")) % p
    except ValueError:
        print("That's not even a number :(")
        sys.exit(1)

    if guess != eval_poly(coefficients, x_val):
        print("Wrong!! I thought you were a math whiz")
        sys.exit(1)

print("Wow, you really are a math whiz! Here's a flag for your troubles:")

# (second arg is not the real flag)
flag = os.getenv("FLAG", "osu{worked-on-local}")
print(flag)
