from sage.all import Zmod, PolynomialRing

import ast

from contools import remote

io = remote("localhost", 8888)

# skip intro
io.recvline()
io.recvline()

# get prime
primeline = io.recvline()
p = int(primeline.split()[7])
print("prime:", p)

F = Zmod(p)
FP = PolynomialRing(F, "x")

for _ in range(50):
    io.recvline()  # here are your coeffs
    coeffs = ast.literal_eval(io.recvline().decode())
    poly = FP(coeffs)

    io.recvline()  # y-value at this point is
    x = F(io.recvline())

    io.sendline(str(poly(x)).encode())

io.recvline()
print(io.recvline().rstrip().decode())
