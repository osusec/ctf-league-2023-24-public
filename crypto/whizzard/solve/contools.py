import socket

class Conn:
    def __init__(self, sock):
        self.sock = sock
        self.buf = bytearray()

    def send(self, msg: bytes):
        total_sent = 0
        msglen = len(msg)
        while total_sent < msglen:
            sent = self.sock.send(msg[total_sent:])
            total_sent += sent

    def sendline(self, msg: bytes):
        self.send(msg + b"\n")

    def recvuntil(self, delim: bytes):
        while delim not in self.buf:
            self.buf += self.sock.recv(4096)

        delim_index = self.buf.index(delim)
        end = delim_index + len(delim)
        message = bytes(self.buf[:end])
        self.buf = self.buf[end:]

        return message

    def recvline(self):
        return self.recvuntil(b"\n")

def remote(url, port):
    sock = socket.create_connection((url, port))
    return Conn(sock)

def process(args):
    pass
