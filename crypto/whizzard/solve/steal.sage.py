from sage.all import crt

from contools import remote

def get_share(io, share):
    io.sendline(str(share).encode())
    share_bytes = io.recvline().split()[-1]
    return int(share_bytes)

io = remote("localhost", 8888)

io.recvline()
io.recvline()

# big shares -> CRT
x1 = pow(2, 256) - 1
x2 = x1 - 1

share1 = get_share(io, x1)
share2 = get_share(io, x2)

flag_int = crt([share1 % x1, share2 % x2], [x1, x2])
flag = flag_int.to_bytes((flag_int.bit_length() + 7) // 8, "big").decode()
print("flag:", flag)
