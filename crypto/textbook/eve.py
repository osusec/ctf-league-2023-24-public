#!/usr/local/bin/python3

from Cryptodome.Util.number import getPrime
from Cryptodome.Cipher import AES

from eve_secret import exponent_a

import secrets
import sys

p = 89570805231151373570869324787052740947845622171858332875724978846294734976509
g = 79293847892739829374923283749872389470283704970293847082972394828394839293874

exponent_b = secrets.randbits(256)

A = pow(g, exponent_a, p)
B = pow(g, exponent_b, p)

print(f"{A = }")
print(f"{B = }")

s = pow(A, exponent_b, p)
assert s == pow(B, exponent_a, p)
s_bytes = s.to_bytes(256 // 8, byteorder="little")

cipher = AES.new(s_bytes, AES.MODE_ECB)

token = secrets.token_bytes(32)
ct = cipher.encrypt(token).hex()

print(f"Encrypted token: {ct}")
print()

print("If you can tell me what I encrypted, I just might give you the flag :)")

try:
    guess = bytes.fromhex(input("Your guess: "))
except ValueError:
    print("You've gotta enter valid hex :(")
    sys.exit(1)

if guess == token:
    print("What??!? How? Well I guess a promise is a promise...")
    with open("flag.txt", "r") as f:
        flag = f.read()
    print(flag)
else:
    print("Not quite, better luck next time I guess :)")
