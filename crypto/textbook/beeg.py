#!/usr/local/bin/python3

from Cryptodome.Util.number import getStrongPrime

with open("flag.txt", "rb") as f:
    FLAG = f.read()

# convert flag to integer for use with RSA
FLAG_INT = int.from_bytes(FLAG, byteorder="little")

# encryption exponent
e = 3

# ensure primes actually work with our public exponent
p = getStrongPrime(4096, e=e)
q = getStrongPrime(4096, e=e)
N = p * q

ciphertext = pow(FLAG_INT, e, N)

print("I've encrypted my flag with my super big RSA key! You don't stand a chance at factoring it so don't even bother trying to decrypt it :)", flush=True)
print()
print("Public modulus:", N)
print("Public exponent:", e)
print("Flag:", ciphertext)
