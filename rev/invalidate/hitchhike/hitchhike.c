#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>

#define FLAG_LEN 52

char FLAG[FLAG_LEN + 1];

char *read_flag() {
  FILE *flag_file = fopen("flag", "r");
  fread(FLAG, 1, FLAG_LEN, flag_file);
  fclose(flag_file);
}

void encrypt_input(char *input, size_t len) {
  for (int i = 0; i < len; i++) {
    input[i] ^= FLAG[i % FLAG_LEN];
  }
}

int main() {
  setbuf(stdin, NULL);
  setbuf(stdout, NULL);
  read_flag();
  memfrob(FLAG, FLAG_LEN);
  
  char input[101];
  printf("Please enter some text to encrypt: ");
  fgets(input, 100, stdin);
  encrypt_input(input, strlen(input));
  printf("Here's that text but encrypted with my super secret key: ");
  fwrite(input, 1, strlen(input), stdout);
  printf("\n");
}
