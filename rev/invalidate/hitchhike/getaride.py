from pwn import *

if __name__ == "__main__":
    context.log_level = "debug"
    # io = process("./a.out")
    io = remote("localhost", 1310)
    
    test_input = b"a"*70
    io.sendline(test_input)
    io.recvuntil(b"key: ")
    enc = io.recvn(min(len(test_input), 99))
    # enc = io.recv()
    print(enc.hex())
    
    dec = xor(enc, test_input[:len(enc)], b"\x2a"*len(enc))
    print(dec)
