import string

# halves = [94, 111, 111, 94, 93, 108, 40, 86, 77, 0, 51, 56, 18, 70, 64, 0]
# shifts = [104, 83, 122, 117, 68, 124, 35, 110, 123, 108, 43, 101, 125, 66, 54, 53]

halves = [6, 104, 40, 67, 71, 44, 107, 107, 59, 80, 70, 25, 52, 107, 90]
shifts = [104, 82, 34, 105, 61, 110, 122, 116, 72, 119, 60, 123, 114, 32, 35, 53]

# first character is a multiple of 11
# first_poss = string.printable
first_poss = [c for c in string.printable if ord(c) % 7 == 0]

# try to recover first half
possibilities = []
for first in first_poss:
    half = first
    fine = True

    for i, h in enumerate(halves):
        next_char = chr(ord(half[i]) ^ halves[i])
        if next_char not in string.printable:
            fine = False
            break
        half += next_char

    if fine:
        possibilities.append(half)

print(possibilities)

# recover rest by shifting?
full = []
for poss in possibilities:
    this = poss
    fine = True

    for char, shift in zip(poss, shifts):
        next_char = chr((ord(char) >> 2) ^ shift)
        if next_char not in string.printable:
            fine = False
            break
        this += next_char

    if fine:
        full.append(this)

print(full)
