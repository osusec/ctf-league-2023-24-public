#include <stdio.h>
#include <string.h>

#define PASSWORD_LEN 32
#define FLAG_LEN 47

int HALF_XORS[] = {6, 104, 40, 67, 71, 44, 107, 107, 59, 80, 70, 25, 52, 107, 90};
int SHIFT_XORS[] = {104, 82, 34, 105, 61, 110, 122, 116, 72, 119, 60, 123, 114, 32, 35, 53};

char FLAG[FLAG_LEN + 1];

/* roundabout way of checking that the password is "17_w4s_4_d4rk_4nd_5t0rmy_n1gh7.." */
int check(char *input) {
  int ok = 1;

  // length check
  ok &= strlen(input) == PASSWORD_LEN;

  // first character hint (n -> 110 in ASCII; 10 possibilities)
  ok &= (input[0] % 7 == 0);

  for (int i = 0; i < PASSWORD_LEN / 2 - 1 && ok; i++) {
    // xors of initial pairs of consecutive characters
    ok &= (input[i] ^ input[i + 1]) == HALF_XORS[i];
  }

  // bit shifts + xors for rest of flag
  for (int i = 0; i < PASSWORD_LEN / 2 && ok; i++) {
    ok &= ((input[i] >> 2) ^ input[i + PASSWORD_LEN / 2]) == SHIFT_XORS[i];
  }

  return ok;
}

void read_flag() {
  FILE *flag_file = fopen("flag", "r");
  fread(FLAG, 1, FLAG_LEN, flag_file);
  fclose(flag_file);
}

int main() {
  setbuf(stdin, NULL);
  setbuf(stdout, NULL);

  printf("Before I give you the flag, you need to tell me the password:\n");
  printf("Password: ");

  char pass[50];
  fgets(pass, 50, stdin);

  // get rid of newline cause yeah
  pass[strlen(pass) - 1] = 0;

  int pass_ok = check(pass);

  if (pass_ok) {
    read_flag();
    printf("That's correct! Here's your flag: %s", FLAG);
  } else {
    printf("Incorrect! No flag for you >:(");
  }
  
  return pass_ok == 1;
}