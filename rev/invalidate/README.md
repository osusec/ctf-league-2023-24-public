# rev/invalidate

Basic idea for each subchallenge/flag:

- `hitchhike`: continuation of last week, just need to look up what memfrob does
- `passxord`: password matches hard-coded constraints; hint should be that it can be typed on a keyboard and makes logical sense
- `supersecure`: first couple can be obtained through just ghidra, third one requires either ctypes or running a local copy of the program
