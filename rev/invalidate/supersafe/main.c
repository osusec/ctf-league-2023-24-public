#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int NUM1 = 1337;
char *NUM2 = "1234";

int num1() {
  // unused rand call to make third number slightly harder (?)
  rand();

  int input;
  printf("First number of combination: ");
  scanf("%d", &input);

  return input == NUM1;
}

int num2() {
  // see above
  rand();

  int input;
  printf("Second number: ");
  scanf("%d", &input);
  

  return strncmp((char *)&input, NUM2, 4);
}

int num3() {
  int target = rand() % 65536;

  int input;
  printf("Third number: ");
  scanf("%d", &input);

  return input == target;
}

void win() {
  printf("With one final click, you feel the safe door start opening. The flag falls into your outstretched hand:\n");

  char flag[100];

  FILE *flag_file = fopen("flag", "r");
  fread(flag, 1, 100, flag_file);
  fclose(flag_file);

  printf("%s\n", flag);
}

int main() {
  // disable input/outbut buffering
  setbuf(stdin, NULL);
  setbuf(stdout, NULL);

  srand(time(NULL));

  printf("Before you lies a safe whose combination changes every second. Inside it lies the flag you've been searching your entire life for, so you feel like you must open it.\n");

  int incorrect1 = !num1();
  int incorrect2 = num2();
  int incorrect3 = !num3();

  if (incorrect1 || incorrect2 || incorrect3) {
    printf("You pull on the safe handle and it doesn't budge. Determined to get the flag, you reset the combination dial and make another attempt.");
  } else {
    win();
  }


  return 0;
}