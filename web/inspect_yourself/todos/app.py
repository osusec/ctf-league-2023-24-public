from base64 import urlsafe_b64encode as b64enc
import textwrap
import os

from flask import Flask, render_template, make_response, send_from_directory, redirect

app = Flask(__name__, static_folder="static")

# TODO: move to text file?
FLAG1 = "osu{7h3_l157_k33p5_g3771ng_l0ng3r}"
FLAG2 = "osu{y0u_can_n3v3r_7ru57_y0ur_3y35}"
FLAG3 = "osu{3n0ugh_0f_y0ur_b4s3l355_cl41ms}"

@app.route("/")
def todo_page():
    resp = send_from_directory(app.static_folder, "todos.html")

    resp.set_cookie("notflag", b64enc(FLAG3.encode("utf-8")).decode())

    return resp

@app.route("/flag")
def rickroll():
    return redirect("https://www.youtube.com/watch?v=dQw4w9WgXcQ")

@app.route("/other")
def flag2():
    return f"""<p style="color: white;">{FLAG2}</p>"""

@app.route("/robots.txt")
def robots_txt():
    return send_from_directory(app.static_folder, "robots.txt")
  
def run():
    app.run()

if __name__ == "__main__":
    run()