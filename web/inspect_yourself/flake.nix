{
  outputs = {self, nixpkgs}:
  let system = "x86_64-linux";
      pkgs = import nixpkgs {inherit system;};
      inherit (pkgs) python3;
      flask-python = python3.withPackages (nixpkgs.lib.attrVals ["flask" "cryptography"]);
  in {
    devShells.${system}.default = pkgs.mkShell {
      packages = [flask-python];
    };

    formatter.${system} = pkgs.alejandra;
  };
}
