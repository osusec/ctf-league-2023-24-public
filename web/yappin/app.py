from flask import Flask, session, request, redirect, render_template
from werkzeug.middleware.proxy_fix import ProxyFix
import yaml

import os
import secrets
import uuid

from classes import User, InitUser, Note


users = {
    str(uuid.uuid4()): User({"user": InitUser("first", [], True)})
}

app = Flask(__name__)
app.secret_key = secrets.token_bytes()

# (this is only for challenge deployment, not relevant to the challenge itself)
if os.getenv("PROD") is not None:
    app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1)

DESTRUCTION_PORTAL = os.getenv("DESTRUCTION_PORTAL", "https://example.com")

@app.route("/")
def main_route():
    # check if logged in (if not, redirect to registration page)
    if (uid := session.get("uid")) is None:
        return redirect("/register")

    # display notes & allow user to create note
    user = users[uid]
    return render_template("notes.html", user=user)

@app.route("/register", methods=["GET", "POST"])
def register():
    # if logged in, redirect to main page
    if session.get("uid") is not None:
        return redirect("/")

    if request.method == "POST":
        try:
            if request.form:
                body = request.form
            else:
                body = yaml.load(request.stream, yaml.Loader)
            user = User(body)

        except Exception as e:
            return "bad registration payload " + str(e), 400

        uid = str(uuid.uuid4())
        session["uid"] = uid

        users[uid] = user
        return redirect("/")
    else:
        return render_template("register.html")


@app.post("/addnote")
def addnote():
    if (uid := session.get("uid")) is None:
        return redirect("/register")

    user = users[uid]

    try:
        if request.form:
            body = request.form
        else:
            body_raw = request.get_data(as_text=True)
            body = yaml.load(body_raw, yaml.Loader)

        note = Note(**body)
    except Exception as e:
        print(e)
        return "bad note", 400

    user.notes.append(note)

    return redirect("/")

@app.route("/admin")
def admin():
    if (uid := session.get("uid")) is None:
        return redirect("/register")

    user = users[uid]
    if not user.is_admin:
        return "admin only zone", 403

    return render_template("admin.html", user=user)

@app.route("/debug_zone")
def debug_flag():
    if (uid := session.get("uid")) is None:
        return redirect("/register")

    user = users[uid]

    if user.priv_level() < 2:
        return "this incident will be reported.", 451

    return render_template("debug.html", user=user)


@app.route("/destruction")
def destruction():
    if (uid := session.get("uid")) is None:
        return redirect("/register")

    if not users[uid].is_admin:
        return "you don't have permission to do that", 403

    return redirect(DESTRUCTION_PORTAL)
