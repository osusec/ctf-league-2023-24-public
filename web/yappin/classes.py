from dataclasses import dataclass

user_attrs = {"username", "notes", "is_admin"}

class User:
    username: str
    notes: [str]
    is_admin: bool = False

    def __init__(self, info):
        userinfo = info.get("user", info)

        if isinstance(userinfo, dict):
            self.username = userinfo["username"]
            self.notes = []
            self.is_admin = False
        elif isinstance(userinfo, InitUser):
            # copy over attributes from other User object
            for attr in user_attrs:
                setattr(self, attr, getattr(userinfo, attr))
        else:
            raise ValueError("bad user info")

        self.priv_level = lambda: int(self.is_admin)
            
        try:
            # TODO: remove before deploying on prod
            for key, val in info.get("debug_args", {}).items():
                # User attrs shouldn't be touched
                if key in user_attrs:
                    continue

                keys = key
                if isinstance(key, str):
                    keys = [key]

                obj = self
                for subkey in keys[:-1]:
                    obj = getattr(obj, subkey)

                setattr(obj, keys[-1], val)
        except AttributeError as e:
            raise ValueError("bad debug args") from e
            
        # sometimes I wish Python was strongly typed :(
        self.is_admin = bool(getattr(self, "is_admin", False))


@dataclass
class InitUser:
    username: str
    notes: list
    is_admin: bool = False

@dataclass
class Note:
    title: str
    contents: str


# TODO: figure out why stuff breaks when this is removed???
code = type((lambda: None).__code__)
