#!/usr/bin/env python3
import requests
from random import randint
from os import getenv
# sanic is basically flask but lets me not feel bad about not using a real WSGI server ;P
from sanic import Sanic, response
from sanic.response import text, html
# http-auth lib by someone
from sanic_httpauth import HTTPBasicAuth
import hashlib

auth = HTTPBasicAuth()
flag = getenv("FLAG")

BOT_HOST = getenv("BOT_HOST")
BOT_PORT = getenv("BOT_PORT")

# securely generate randomized password
admin_password = "".join([chr(randint(65,90)) for _ in range(10)])

app = Sanic("BingoAI")

def hash_password(salt, password):
    salted = password + salt
    return hashlib.sha512(salted.encode("utf8")).hexdigest()

app_salt = "NaCl"

# I'm so good at security! Password salting and hashing! Muahahaha
users = {"admin": hash_password(app_salt, admin_password)}


data_dict = {}  # Store data in memory



@auth.verify_password
def verify_password(username, password):
    if username in users:
        return users.get(username) == hash_password(app_salt, password)
    return False


@app.get("/login")
@auth.login_required
async def login(request):
    response = text("Login successful!")
    # add cookie so I stay logged in
    response.add_cookie(
        "isAdmin",
        "True",
        path="/",
        httponly=True,
        secure=False
    )
    return response


@app.get("/enhanced_internet_search")
async def enhanced_internet_search(request):

    admin_cookie = request.cookies.get("isAdmin")
    if admin_cookie and admin_cookie == "True":

        address = request.args.get("address")
        if address is None:
            return text("You must specify an address")

        r = requests.get(f"http://{BOT_HOST}:{BOT_PORT}", {
            "address":address
        })

        return html(r.text)
    else:
        return html("<b>Admin only!<b>")

# Route to handle the form submission
@app.route('/submit', methods=['POST'])
async def submit(request):
    text = request.form.get('text')

    # Generate a unique ID to use as the key for the data_dict
    unique_id = len(data_dict) + 1

    # Store the submitted text in data_dict with the unique ID as the key
    data_dict[unique_id] = text

    return response.redirect(f'/id/{unique_id}')

# Route to display the submitted text
@app.route('/id/<id:int>')
async def display_text(request, id):
    text = data_dict.get(id, 'Not found')

    # Return a simple HTML page with the submitted text
    return response.html(f'<html><body><h1>Submitted Text:</h1><p>{text}</p></body></html>')


@app.get("/")
async def access(request):
    # check request cookie
    admin_cookie = request.cookies.get("isAdmin")
    if admin_cookie and admin_cookie == "True":
        return html(flag + "\n\n\n" + open("admin.html").read())
    response = html(open("index.html").read())
    return response


if __name__ == "__main__":
    print("**Launching BingoAI v1.00**")
    app.run(host='0.0.0.0', port=3003)
