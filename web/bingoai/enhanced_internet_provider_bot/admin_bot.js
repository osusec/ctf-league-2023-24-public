const http = require('http');
const puppeteer = require('puppeteer');

SECRET = process.env.SECRET
OTHER_SECRET = process.env.OTHER_SECRET

const server = http.createServer(async (req, res) => {
  const { url } = req;
  const urlParts = new URL(url, `http://${req.headers.host}`);
  const address = urlParts.searchParams.get('address');
  
  if (!address) {
    res.writeHead(400, { 'Content-Type': 'text/plain' });
    res.end('Address parameter missing');
    return;
  }

  try {
    const browser = await puppeteer.launch({ headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox'] });
    const page = await browser.newPage();

    console.log(`Visiting address: ${address}`);

    const host = new URL(address).host;
    console.log(host)

    page.setCookie({ name: "SECRET", value: OTHER_SECRET, domain: `bingoai.ctf-league.osusec.org/` })
    
    page.setExtraHTTPHeaders({
        "TOTALLY_NOT_A_FLAG":`${SECRET}`
    })
    
    await page.goto(address);

    html = await page.content()
    console.log("Visited address successfully");

    await browser.close();

    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end(html);
  } catch (error) {
    console.error("Error occurred:", error);
    res.writeHead(500, { 'Content-Type': 'text/plain' });
    res.end('An error occurred');
  }
});

const PORT = 9000;
server.listen(PORT, () => {
  console.log(`Admin bot server running at http://localhost:${PORT}`);
});
