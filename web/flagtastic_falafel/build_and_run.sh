#!/bin/sh

docker stop flagtastic_falafel || true
docker rm flagtastic_falafel || true
docker build . -t flagtastic_falafel
docker run --name flagtastic_falafel -p 5001:80 -d flagtastic_falafel

