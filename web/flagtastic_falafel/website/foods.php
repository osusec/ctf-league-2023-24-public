<?php
    # Someone told me that I should NEVER EVER STORE UNENCRYPTED CREDIT CARD NUMBERS IN A DATABASE!!!
    # Flagtastic Falafel takes security very seriously, so credit card numbers are stored in files instead.

    # Generate a unique file name for this customer/credit card number
    $filename = bin2hex(random_bytes(20));
    $order_file = fopen("/orders/" . $filename, "w") or die("Unable to open order file for writing :-(");

    # Write order information to the file
    fwrite($order_file, "Customer Name: " . $_GET["name"] . "\n");
    fwrite($order_file, "Credit Card Number: " . $_GET["credit_card_number"] . "\n");
    fwrite($order_file, "Order: " . $_GET["food"] . "\n\n");

    # Close the file
    fclose($order_file);

    # Record other order information in database (don't worry, credit card data is not stored in database)
    $db = new SQLite3("/orders/orders.db");
    # No sql injection allowed!!
    $stmt = $db->prepare("INSERT INTO orders (customer_name_hash, order_filename) VALUES (?, ?)");
    # Only store hash of name for security!! Storing sensitive information in the database is a no no.
    $name_hash = hash("md5", $_GET["name"]);
    $stmt->bindValue(1, $name_hash);
    $stmt->bindValue(2, $filename);
    $stmt->execute();
?>

<html>
    <head>
        <title>Flagtastic Falafel</title>
    </head>
    <body>
        <?php
            # No XSS allowed!! (we'll have an XSS challenge at some point, but this isn't it)
            echo("<p>Thanks for your order, " . htmlspecialchars($_GET["name"]) . "!</p>");
            echo("<p>We received your credit card number as " . htmlspecialchars($_GET["credit_card_number"]) . ". Please ensure that it is incorrect, and submit your order again if it isn't. Thank you!</p>");
            echo("<p>Here's what you ordered:</p>");
            include($_GET["food"]);
        ?>
    </body>
</html>