<html>
    <head>
        <title>Flagtastic Falafel</title>
    </head>
    <body>
        <img src="images/flagtastic.png">
        <h1>Welcome to Flagtastic Falafel!</h1>
        <p>What food do you want to order?</p>
        <form action="foods.php" type="GET" style="display: block;">
            <label><input type="radio" name="food" value="fabled.php">Fabled Falafel</label><br>
            <label><input type="radio" name="food" value="fake.php">Fake Falafel</label><br>
            <label><input type="radio" name="food" value="famous.php">Famous Falafel</label><br>
            <label><input type="radio" name="food" value="fancy.php">Fancy Falafel</label><br>
            <label><input type="radio" name="food" value="fantastic.php">Fantastic Falafel</label><br>
            <label><input type="radio" name="food" value="fashionable.php">Fashionable Falafel</label><br>
            <label><input type="radio" name="food" value="fast.php">Fast Falafel</label><br>
            <label><input type="radio" name="food" value="fattening.php">Fattening Falafel</label><br>
            <label><input type="radio" name="food" value="ferromagnetic.php">Ferromagnetic Falafel</label><br>
            <label><input type="radio" name="food" value="festive.php">Festive Falafel</label><br>
            <label><input type="radio" name="food" value="fetid.php">Fetid Falafel</label><br>
            <label><input type="radio" name="food" value="fibrous.php">Fibrous Falafel</label><br>
            <label><input type="radio" name="food" value="fizzy.php">Fizzy Falafel</label><br>
            <label><input type="radio" name="food" value="flaming.php">Flaming Falafel</label><br>
            <label><input type="radio" name="food" value="freaky.php">Freaky Falafel</label><br>
            <label><input type="radio" name="food" value="fresh.php">Fresh Falafel</label><br>
            <label><input type="radio" name="food" value="frightening.php">Frightening Falafel</label><br>
            <label><input type="radio" name="food" value="frijoles.php">Frijoles Falafel</label><br>
            <label><input type="radio" name="food" value="frosty.php">Frosty Falafel</label><br>
            <label><input type="radio" name="food" value="frozen.php">Frozen Falafel</label><br>
            <label><input type="radio" name="food" value="fruit.php">Fruit Falafel</label><br>
            <label><input type="radio" name="food" value="fungal.php">Fungal Falafel</label><br>
            <label><input type="radio" name="food" value="futuristic.php">Futuristic Falafel</label><br>
            <label><input type="radio" name="food" value="fuzzy.php">Fuzzy Falafel</label><br>
            <label for="name_input">Name</label>
            <input type="text" id="name_input" name="name"></input><br>
            <label for="credit_card_number_input">Credit Card Number</label>
            <input type="text" id="credit_card_number_input" name="credit_card_number"></input><br>
            <button type="submit">Place order</button>
        </form>
    </body>
</html>
