<?php
    $db = new SQLite3('/orders/orders.db');
    $db->exec("DROP TABLE IF EXISTS orders;");
    $db->exec("
    CREATE TABLE orders (
        id INTEGER PRIMARY KEY,
        customer_name_hash VARCHAR(255),
        order_filename VARCHAR(255),
        order_date DATETIME DEFAULT CURRENT_TIMESTAMP
    );
    ");
?>