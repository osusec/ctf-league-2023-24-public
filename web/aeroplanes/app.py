from dataclasses import dataclass

from flask import Flask, render_template, request, session, redirect

import os
from markdown import markdown
import urllib.parse
import uuid
import sys
import subprocess
import requests


ADMIN_USERNAME: str = os.environ.get("ADMIN_USERNAME")
ADMIN_PASSWORD: str = os.environ.get("ADMIN_PASSWORD")
FLAG_X: str = os.environ.get("FLAG_X")
FLAG_Y: str = os.environ.get("FLAG_Y")

if not ADMIN_USERNAME or not ADMIN_PASSWORD or not FLAG_X or not FLAG_Y:
    print("Did not specify one of the environment variables - exiting")
    sys.exit(1)

app = Flask(__name__)
app.config['DEBUG'] = True
app.secret_key = os.environ.get("SECRET_KEY") or '_5#y2L"F4Q8z\n\xec]/'
app.config["SESSION_COOKIE_SAMESITE"] = "Lax"

@app.after_request
def apply_csp(response):
    response.headers[
        "Content-Security-Policy"
    ] = "default-src 'self'; img-src *; connect-src *; script-src data:; style-src 'self' 'unsafe-inline';"
        
    return response

def generate_pid():
    return str(uuid.uuid4())

@dataclass
class User:
    username: str
    password: str

users: list[User] = []

def add_user(username: str, password: str) -> User:
    users.append(User(username, password))
    return users[-1]


ADMIN = add_user(ADMIN_USERNAME, ADMIN_PASSWORD)

@dataclass
class Aeroplane:
    pid: str
    name: str
    year: int
    image: str
    description: str
    public: bool
    user: User

database: dict[str, Aeroplane] = dict()

def add_plane(name: str, year: int, image: str, description: str, public = True, user = ADMIN) -> Aeroplane:
    pid = generate_pid()
    new_plane = Aeroplane(pid, name, year, image, description, public, user)
    database[pid] = new_plane
    return new_plane

add_plane("Macchi M33", 1925, "https://community.gamedev.tv/uploads/db2322/original/3X/f/f/ff052828d86f0fe35554ae4f32c37a37c7a13967.jpg", "Sorry baby, gotta fly")
add_plane("Spruce Goose", 1947, "https://www.ocregister.com/wp-content/uploads/2017/11/spruce-goose-top.gif?w=711", "Airborne for about 30 seconds, 25 feet above the harbor for just under half a mile! Visit it at the Evergreen Museum!")
add_plane("The Darkstar", 2025, "https://robbreport.com/wp-content/uploads/2022/06/Lockheed_Darkstar_1.jpeg", "Remembers boys, no points for seconds place")
add_plane("C-17", 1993, "https://static1.thegamerimages.com/wordpress/wp-content/uploads/2020/03/Call-of-Duty-Warzone-map-locations.jpg?q=50&fit=contain&w=1140&h=570&dpr=1.5",  "Where we dropping boys?")
add_plane("A5M", 1969, "https://upload.wikimedia.org/wikipedia/commons/e/eb/British_Airways_Concorde_G-BOAC_03.jpg", "A droop that snoots")

add_plane("FLAG_Y", 
          -1024, 
          "https://t4.ftcdn.net/jpg/01/63/79/03/360_F_163790330_smiPqhO55KQHR18IciNWyBHkCMgZXM1F.jpg", 
          FLAG_Y, 
          public=False, 
          user=ADMIN)

@app.get("/")
@app.get("/index")
@app.get("/home")
def index():
    search = request.args.get("search")

    values: list[Aeroplane] = []

    # Display all public planes, as well as the ones we have personally added
    if search is None:
        for key, val in database.items():
            if val.public or (session.get("username") == val.user.username) and session.get("password") == val.user.password:
                values.append(val)
    else:
        for key, val in database.items():
    
            if search in val.name and (val.public or (session.get("username") == val.user.username) and session.get("password") == val.user.password):
                values.append(val)
    
    return render_template("index.html", planes=values, query=search)


@app.post("/search")
def search():
    search = request.form.get("search")
    return redirect("/?search=" + urllib.parse.quote_plus(search))


@app.post("/login")
def login():
    username = request.form.get("username")
    password = request.form.get("password")

    if username is None or password is None:
        return redirect(
            "/login?error="
            + urllib.parse.quote_plus("Need both username and password.")
        )
    
    found_user = False
    for user in users:
        if username == user.username:
            if password != user.password:
                return redirect(
                    "/login?error="
                    + urllib.parse.quote_plus("Incorrect password !")
                )
            else:
                found_user = True
                break
            
    # Not pre-existing username
    if not found_user:
        add_user(username, password)

    session["username"] = username
    session["password"] = password

    return redirect("/")

@app.get("/login")
def login_page():
    if session.get("username") is not None and session.get("password") is not None:
        return redirect("/")

    error = request.args.get("error")
    return render_template("login.html", error=error)

@app.get("/logout")
def logout():
    session.pop("username", None)
    session.pop("password", None)
    return redirect("/")

@app.get("/view/<pid>")
def page(pid):
    if pid is None:
        return redirect(
            "/404?error=" + urllib.parse.quote_plus("Need to specify a pid.")
        )

    if pid not in database:
        return redirect("/404?error=" + urllib.parse.quote_plus("Plane not found."))

    plane = database[pid]

    return render_template(
        "view.html",
        pid=plane.pid,
        name=plane.name,
        image=plane.image,
        year=plane.year,
        description=markdown(plane.description),
        own=plane.user.username == session.get("username") and plane.user.password == session.get("password")
    )


@app.get("/edit/<pid>")
def edit_page(pid):
    if session.get("username") is None or session.get("password") is None:
        return redirect(f"/view/{pid}")

    if pid not in database:
        return redirect("/create?error=" + urllib.parse.quote_plus("Plane not found."))

    plane = database[pid]

    if plane.user.username != session.get("username") or plane.user.password != session.get("password"):
        return redirect("/create?error=" + urllib.parse.quote_plus("You don't own that plane!"))

    return render_template(
        "edit.html",
        id=plane.pid,
        name=plane.name,
        image=plane.image,
        year=plane.year,
        description=plane.description,
    )


@app.post("/edit/<pid>")
def edit_api(pid):
    if session.get("username") is None or session.get("password") is None:
        return redirect(f"/view/{pid}")


    name = request.form.get("name")
    year = request.form.get("year")
    image = request.form.get("image")
    description = request.form.get("description")

    if (
        name is None
        or year is None
        or image is None
        or description is None
    ):
        return redirect(
            f"/edit/{pid}?error=" + urllib.parse.quote_plus("Need all fields.")
        )

    if pid in database:
        plane = database[pid]

        if plane.user.username != session.get("username") or plane.user.password != session.get("password"):
            return redirect("/create?error=" + urllib.parse.quote_plus("You don't own that plane!"))
        
        plane.name = name 
        plane.year = int(year)
        plane.image = image
        plane.description = description

    return redirect(f"/view/{pid}")


@app.get("/create")
def new_page():
    if session.get("username") is None or session.get("password") is None:
        return redirect("/login?error=" + urllib.parse.quote_plus("Not logged in."))

    error = request.args.get("error")
    return render_template("create.html", error=error)

@app.post("/create")
def create_page():
    if session.get("username") is None or session.get("password") is None:
        return redirect("/login?error=" + urllib.parse.quote_plus("Not logged in."))

    title = request.form.get("name")
    year = request.form.get("year")
    image = request.form.get("image")
    description = request.form.get("description")

    if (
        title is None
        or image is None
        or year is None
        or description is None
    ):
        return redirect("/create?error=" + urllib.parse.quote_plus("Need all fields."))

    new_plane = add_plane(
        title,
        2000,
        image,
        description,
        public=False,
        user=User(session.get("username"),session.get("password"))
    )
    
    return redirect(f"/view/{new_plane.pid}")


@app.delete("/delete/<pid>")
def delete_page(pid):
    if session.get("username") is None or session.get("password") is None:
        return redirect("/login?error=" + urllib.parse.quote_plus("Not logged in."))

    if pid in database:
        del database[pid]
   
    return redirect("/")


@app.post("/flag")
def flag():
    adminpw = os.environ.get("ADMINPW") or "admin"
    if session.get("password") != adminpw:
        return redirect("/login?error=" + urllib.parse.quote_plus("Not the admin."))

    flag = os.environ.get("FLAG") or "osu{test_flag_not_real}"
    return flag, 200


@app.get("/bot")
def get_bot():
    result = request.args.get("result")
    return render_template("bot.html", string=result)


@app.post("/bot")
def post_bot():
    URL = request.form.get("url")

    if not URL:
        return redirect(
            "/login?error="
            + urllib.parse.quote_plus("Pass in a URL!")
        )
    
    url = urllib.parse.urlparse(URL)

    ret = requests.get(f"http://west.forestofnewt.net:8000/view?url={url.path}")

    return redirect("/bot?result=" + urllib.parse.quote_plus("Admin 'bot' will be visiting the URL"))

@app.errorhandler(404)
def page_not_found(_):
    error = request.args.get("error") or "Page not found"
    return render_template("404.html", error=error), 404
