from base64 import urlsafe_b64decode
import hashlib
import hmac
import json
import os
import pathlib
import subprocess
import tempfile
import uuid

from flask import (
    Flask,
    render_template,
    request,
    redirect,
    after_this_request,
    make_response,
)
import jwt
from werkzeug.middleware.proxy_fix import ProxyFix

app = Flask(__name__)

# (this is irrelevant to challenge, ignore)
if os.getenv("PROD") is not None:
    app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1)

# read signing key from disk
signing_key_path = pathlib.Path("jwt-key.pem")

with open(signing_key_path) as sk:
    SIGNING_KEY = sk.read()

# no stealing our signing secrets >:(
signing_key_path.unlink()

# also read verification key into memory
with open("jwt-key.pem.pub") as pk:
    VERIFICATION_KEY = pk.read()

# create upload directory
pathlib.Path("uploads").mkdir()

# add admin flag directory
ADMIN_UUID = uuid.uuid4()
admin_path = pathlib.Path("uploads") / str(ADMIN_UUID)

admin_path.mkdir()

# write flag to admin dir
FLAG = os.getenv("FLAG", "osu{m4yb3_7ry_0n_r3m073?}")
with open(admin_path / "flag.txt", "w") as flag:
    flag.write(FLAG)


def generate_token():
    """generates a session token JWT"""
    uid = uuid.uuid4()
    return jwt.encode({"uid": str(uid), "admin": False}, SIGNING_KEY, algorithm="RS256")


def urlsafe_b64pad(unpadded):
    """pads url-safe base64 properly cause Python is annoying"""
    padded = unpadded
    remaining = len(unpadded) % 4
    if remaining != 0:
        padded += "=" * (4 - remaining)

    return padded


def validate_token(token):
    """Validates a JSON session token"""
    try:
        body = jwt.decode(token, VERIFICATION_KEY, algorithms=["RS256", "HS256"])
    except (
        jwt.exceptions.InvalidSignatureError,
        jwt.exceptions.DecodeError,
    ):
        return False
    except jwt.exceptions.InvalidKeyError:
        # TODO (Casey): figure out why legacy HS256 signatures fail
        # this is a hacky reimplementation of HS256 for now
        parts = token.split(".")
        body = ".".join(parts[:2])
        digest = hmac.digest(
            VERIFICATION_KEY.encode(), body.encode(), digest=hashlib.sha256
        )

        # correct for base64 padding
        provided_sig = urlsafe_b64pad(parts[2])

        if hmac.compare_digest(digest, urlsafe_b64decode(provided_sig)):
            body = json.loads(urlsafe_b64decode(urlsafe_b64pad(parts[1])))
        else:
            return False

    # ensure uid is present and a valid uuid
    try:
        _ = uuid.UUID(body["uid"])
    except (ValueError, KeyError):
        return False

    # gotta make extra sure no one can access admin files
    # body["uid"] is guaranteed to exist at this point
    if body["uid"] == str(ADMIN_UUID):
        return False

    return body


@app.before_request
def ensure_token():
    token = request.cookies.get("token")
    if token is None:
        # generate new token for user
        new_token = generate_token()

        # set cookie on response
        @after_this_request
        def set_token(resp):
            resp.set_cookie("token", new_token)
            return resp

    else:
        # ensure token is valid; otherwise respond with error
        if not validate_token(token):
            resp = make_response(("Invalid token", 403))
            return resp


@app.get("/")
def upload_redirect():
    return redirect("/upload")


@app.get("/upload")
def upload_page():
    return render_template("upload.html")


@app.post("/upload")
def upload_post():
    # guaranteed to be valid here (see before_response)
    token = request.cookies.get("token")
    contents = validate_token(token)
    uid = contents["uid"]

    if not unzip_file(request.files["zip"], uid):
        return ("error: bad zip file", 400)

    return redirect(f"/browse/{uid}")


@app.get("/browse/<uid>")
def browse_contents(uid):
    # ensure session token is valid & has correct uid
    token = request.cookies.get("token")
    contents = validate_token(token)
    if uid != contents["uid"]:
        return ("error: invalid uid", 403)

    # render page with directory contents
    user_dir = pathlib.Path("uploads") / uid
    return render_template("browse.html", uid=uid, children=user_dir.iterdir())


@app.get("/files/<uid>/<file>")
def get_file(uid, file):
    # ensure session token valid & accessed uid matches session
    token = request.cookies.get("token")
    contents = validate_token(token)
    if uid != contents["uid"]:
        return ("error: invalid uid", 403)

    # no directory traversal (probably unnecessary but eh)
    if "." in file:
        return ("lol. lmao.", 403)

    # get file path
    user_dir = pathlib.Path("uploads") / uid
    file_path = user_dir / file

    # verify absolute path is in /app (no reading /etc/shadow for you)
    try:
        resolved = file_path.resolve(strict=True)
    except FileNotFoundError:
        return ("error: file not found", 404)

    if not str(resolved).startswith("/app"):
        return ("nice try lol", 403)

    if not file_path.is_file():
        return ("sorry, archive subdirectories aren't supported yet", 400)

    # attempt to read file contents
    try:
        with open(file_path, "rb") as f:
            return f.read()
    except FileNotFoundError:
        return ("error: file not found", 404)


@app.get("/status")
def status_page():
    # ensure user is admin
    token = request.cookies.get("token")
    contents = validate_token(token)
    if not contents or not contents["admin"]:
        return "wait you're not an admin", 403

    # determine uptime
    uptime = subprocess.run(["uptime"], capture_output=True).stdout.decode()

    # get some system info
    uname = subprocess.run(["uname", "-a"], capture_output=True).stdout.decode()

    return render_template(
        "status.html", uptime=uptime, uname=uname, admin_uuid=str(ADMIN_UUID)
    )


def rmdir_recurse(dirpath):
    for child in dirpath.iterdir():
        if child.is_dir():
            # recurse into subdirectories
            rmdir_recurse(child)
        else:
            # remove normal files
            child.unlink()

    # remove directory itself
    dirpath.rmdir()


def unzip_file(zip, uid):
    # remove dir if it already exists
    user_dir = pathlib.Path(f"uploads") / uid
    if user_dir.exists():
        rmdir_recurse(user_dir)

    # save file to disk
    with tempfile.NamedTemporaryFile() as tmp:
        zip.save(tmp)

        # this is necessary for some reason??
        tmp.seek(0)

        # unzip file using command line utility
        try:
            subprocess.run(["unzip", tmp.name, "-d", f"uploads/{uid}"], check=True)
            return True
        except subprocess.CalledProcessError:
            return False
