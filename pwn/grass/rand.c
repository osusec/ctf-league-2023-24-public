#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <fcntl.h>
#include <unistd.h>

// see https://elixir.bootlin.com/glibc/latest/source/stdlib/random.c
struct random_data lcg_state = {
  .rand_type = 0, /* TYPE_0 */
  .rand_deg = 0, /* DEG_0 */
  .rand_sep = 0, /* SEP_0 */
  .state = NULL, // to be set later

  // these are unused
  .fptr = NULL,
  .rptr = NULL
};

int main() {
  int state_buf = time(NULL);
  lcg_state.state = &state_buf;

  int statate;
  int urandom = open("/dev/urandom", O_RDONLY);
  read(urandom, &statate, sizeof(statate));
  close(urandom);

  srandom_r(statate, &lcg_state);

  for (int i = 0; i < 10; i++) {
    int randout;
    random_r(&lcg_state, &randout);
    printf("%d\n", randout >> 8);
  }

  return 0;
}
