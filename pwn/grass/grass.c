#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// see https://elixir.bootlin.com/glibc/latest/source/stdlib/random.c
int LCG_INT_STATE = 0;
struct random_data LCG_STATE = {.rand_type = 0, /* TYPE_0 */
                                .rand_deg = 0,  /* DEG_0 */
                                .rand_sep = 0,  /* SEP_0 */
                                .state = &LCG_INT_STATE,

                                // these are unused
                                .fptr = NULL,
                                .rptr = NULL,
                                .end_ptr = NULL};

#define STACK_SIZE 200

struct grass_stack {
  int stack_contents[STACK_SIZE];
  int *stack_top;
};

typedef struct grass_stack *stackp;

int pop_arg(stackp stack) {
  // ensure pointer doesn't go below bottom of stack
  if (stack->stack_top != stack->stack_contents) {
    --stack->stack_top;
  }

  // return value at top
  return *stack->stack_top;
}

void stack_push(stackp stack, int val) {
  *stack->stack_top = val;
  ++stack->stack_top;
}

int read_num() {
  char *line;
  size_t alloc_len = 0;
  ssize_t line_len = getline(&line, &alloc_len, stdin);

  if (line_len != 1) {
    char *endptr;
    int res = strtol(line, &endptr, 10);
    free(line);

    return res;
  } else {
    return -1;
  }
}

void fun() {
  puts("If you can guess three random numbers in a row you just might get a "
       "little prize :)");
  int guesses = 3;

  for (int g = 0; g < 3; g++) {
    int real_num;
    random_r(&LCG_STATE, &real_num);
    real_num >>= 16;

    printf("Your guess: ");
    int guess = read_num();

    if (guess != real_num) {
      puts("Nice try! Not correct though");
      exit(1);
    }
  }

  char buf[100];
  int flagfd = open("fun_flag", O_RDONLY);

  if (flagfd == -1) {
    strcpy(buf, "osu{0n_l0cal_f4k3_fun_fl4g}");
  } else {
    read(flagfd, buf, sizeof(buf));
    close(flagfd);
  }

  printf("Nice job! Your flag is %s\n", buf);
}

void hidden_flag() {
  int flagfd = open("hidden_flag", O_RDONLY);
  char buf[100];

  if (flagfd == -1) {
    strcpy(buf, "osu{h1dd3n_fl4g_bu7_0n_l0c4l}");
  } else {
    read(flagfd, buf, sizeof(buf));
    close(flagfd);
  }

  printf(
      "Wowow! Not sure how but you found the hidden flag so here it is: %s\n",
      buf);
}

void run_grass(char *grass_code, int code_len) {
  // initialize stack
  struct grass_stack stack;
  memset(&stack, 0, sizeof(stack));
  stack.stack_top = stack.stack_contents;

  // do execution :)
  char *instruction_ptr = grass_code;

  int running = 1;

  while (running && instruction_ptr < grass_code + code_len) {
    // yay pointer hacks :]
    char instruction = *instruction_ptr;
    int arg = *(int *)(instruction_ptr + 1);
    int stack_arg1, stack_arg2;

    switch (instruction) {
    case 1:
      // add
      stack_arg1 = pop_arg(&stack);
      stack_arg2 = pop_arg(&stack);

      stack_push(&stack, stack_arg1 + stack_arg2);
      break;

    case 2:
      // sub
      stack_arg1 = pop_arg(&stack);
      stack_arg2 = pop_arg(&stack);

      stack_push(&stack, stack_arg1 - stack_arg2);
      break;

    case 3:
      // mul
      stack_arg1 = pop_arg(&stack);
      stack_arg2 = pop_arg(&stack);

      stack_push(&stack, stack_arg1 * stack_arg2);
      break;

    case 4:
      // div
      stack_arg1 = pop_arg(&stack);
      stack_arg2 = pop_arg(&stack);

      stack_push(&stack, stack_arg1 / stack_arg2);
      break;

    case 5:
      // mod
      stack_arg1 = pop_arg(&stack);
      stack_arg2 = pop_arg(&stack);

      stack_push(&stack, stack_arg1 % stack_arg2);
      break;

    case 6:
      // negate
      *(stack.stack_top - 1) = -*(stack.stack_top - 1);
      break;

    case 16:
      // display
      stack_arg1 = pop_arg(&stack);
      printf("%d\n", stack_arg1);
      break;

    case 17:
      // debug peek (thing /at/ stack ptr - allows for leaking canary)
      printf("%d\n", *stack.stack_top);
      break;

    case 18:
      // push
      stack_push(&stack, arg);
      break;

    case 33:
      // rand
      random_r(&LCG_STATE, &stack_arg1);
      stack_arg1 >>= 16;
      printf("%d\n", stack_arg1);
      break;

    case 34:
      // fun (random number guessing game)
      fun();
      break;

    case 99:
      // exit
      running = 0;
      break;

    default:
      puts("INVALID INSTRUCTION - EXITING");
      running = 0;
    }

    instruction_ptr += 5;
  }
}

void grass_init() {
  // buffering is annoying
  setbuf(stdin, NULL);
  setbuf(stdout, NULL);

  // read rng seed from urandom
  int randfd = open("/dev/urandom", O_RDONLY);
  int seed;
  read(randfd, &seed, sizeof(seed));
  close(randfd);

  // actually seed global rng
  srandom_r(seed, &LCG_STATE);
}

int main() {
  char *code = NULL;
  size_t code_alloc_size = 0;
  ssize_t input_len;

  grass_init();

  puts("Welcome to GRASS! Please enter your GVM (GRASS VM) bytecodea after the "
       "> to be evaluated.");
  printf("> ");

  while ((input_len = getline(&code, &code_alloc_size, stdin)) > 1) {
    // ignore trailing newline delimiter
    int code_len = input_len - 1;

    // hacky modulo check to avoid funky optimizations (hopefully)
    if ((code_len / 5) * 5 != code_len) {
      puts("INVALID GRASS PROGRAM");
      return 1;
    } else {
      run_grass(code, code_len);
    }

    printf("> ");
  }

  return 0;
}
