#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

void read_flag(){
    printf("%s\n", getenv("FLAG"));
}

int AMOUNT = 250;

int main()
{
    // Disable buffering
    setbuf(stdout, NULL);
    setbuf(stdin, NULL);
    time_t t;
    srand((unsigned) time(&t));

    int target_number = rand() % 60000 + 50;

    printf("On the next line, I giving you a number. Send that number to me %d times\n", AMOUNT);
    printf("%d\n", target_number);

    int count = 0;
    int n = AMOUNT;

    while(n--){
        int input;
        scanf("%d", &input);

        if(input != target_number){
            printf("Wrong number!\n");
            return 1;
        } else {
            count += 1;
        }
    }

    if(count == AMOUNT){
        printf("Alright, here's my flag: ");
        read_flag();
        printf("\n");
        return 0;
    }

    printf("Try again!\n");
}


