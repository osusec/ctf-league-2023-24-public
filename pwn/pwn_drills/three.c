#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

void read_flag(){
    printf("%s", getenv("FLAG"));
}

const int NUMBER = 1337;
int AMOUNT = 100;

int main()
{
    setbuf(stdout, NULL);
    setbuf(stdin, NULL);
    time_t t;
    srand((unsigned) time(&t));

    printf("Starting on the next line, I will be giving you %d random numbers, one my one. For each number, repeat it back to me!\n", AMOUNT);
    int n = AMOUNT;
    int count = 0;

    while(n--){

        int num = rand();
        printf("%d\n", num);

        int input;
        scanf("%d", &input);

        if(input != num){
            printf("Wrong number!!\n");
            return 1;
        } else {
            count++;
        }
    }

    if(count == AMOUNT){
        printf("Alright, here's my flag: ");
        read_flag();
        printf("\n");
        return 0;
    }

    printf("Try again!\n");
}


