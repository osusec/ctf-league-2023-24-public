#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

void read_flag(){
    printf("%s\n", getenv("FLAG"));
}

const int NUMBER = 1337;
int AMOUNT = 250;

int main()
{
    // Disable buffering
    setbuf(stdout, NULL);
    setbuf(stdin, NULL);

    printf("Use pwntools to send the constant number '%d' to me %d times (each on a seperate line)\n", NUMBER, AMOUNT);    

    int count = 0;

    int n = AMOUNT;

    while(n--){

        int input;
        scanf("%d", &input);

        if(input != NUMBER){
            printf("Wrong number!\n");
            return 1;
        } else {
            count += 1;
        }
    }

    if(count == AMOUNT){
        printf("Alright, here's my flag: ");
        read_flag();
        printf("\n");
        return 0;
    }

    printf("Try again!\n");
}


