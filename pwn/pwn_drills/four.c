#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

void read_flag(){
    printf("%s", getenv("FLAG"));
}

int main()
{
    setbuf(stdout, NULL);
    setbuf(stdin, NULL);

    int stack_variable;
    int* ptr = &stack_variable;

    printf("I am going to read from memory and send you the raw bytes - 64-bit wide. Return to me the decimal string representation of this little-endian value\n");
    
    write(1,&ptr,8);

    long long first;
    scanf("%lld", &first);
    
    if(ptr == first){
        printf("Alright, here's my flag: ");
        read_flag();
        printf("\n");
    } else {
        printf("Try again!\n");
    }

    return 0;
}


