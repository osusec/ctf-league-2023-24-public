#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>


char text[] = "Ok, here is the flag";
void read_flag(){
    printf("%s: %s\n", text, getenv("FLAG"));
}

int main()
{
    setbuf(stdout, NULL);
    setbuf(stdin, NULL);


    printf("Now that you are a pwntools master, lets do some pwning!!\n");
    printf("Lo, our program hath bestowed upon thee a curious gift - a single primitive, akin to the ancient magicks of 'write-what-where'. Behold, as the Global Offset Table bends to thy whims.\n");

    printf("1. I'm ready.\n");
    printf("2. Nah, just give me the flag.\n");
    printf("3. Quit.\n");

    printf("Pick an option: ");
    int option;
    scanf("%d", &option);

    switch(option){
        case 1:{
            printf("Send me the raw address to write to: ");
            unsigned long long int addr;
            read(0,&addr,8);
            printf("Send me the raw value to write to this address: ");
            
            unsigned long long int value;
            read(0,&value,8);

            unsigned long long int* a = (void*) addr;
            *a = value;

            break;
        }
        case 2:{
            printf("%s\n", "https://i.kym-cdn.com/photos/images/original/002/621/765/0da.gif");
            break;
        }
        case 3:{
            break;
        }
    }

    printf("Peace!\n");

}


