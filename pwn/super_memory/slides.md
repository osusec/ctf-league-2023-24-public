---
title:  Overflows and Underflows
author: Paul Simko
date:   November 27, 2023
theme:  white
transition: slide
progress: false
controls: false
---

---

## Or, The over and under on various flows

### Or, Where do numbers go after I put them in the computer?

---

## Where do numbers go after I put them in the computer?

* Into memory
* At some address
* Converted to bits

# How many bits?

* It depends
* A character is 8 bits
* 8 bits is a byte
* A byte can be [0, 255]
* or [-128, 127] if it's signed
* A character is a byte

# Can numbers be bigger?

* Use multiple bytes
* ints have 4 bytes
* [0, 4294967295] [0x0, 0xffffffff]
* [-2147483648, 2147483647]

--- 

## Interlude: Hexadecimal

* Hackers call it hex
* Nerds call it base 16
* 0123456789abcdef = 16 digits instead of 10
* Prefix with 0x
* Two hex digits = byte
* 0xa = 10
* 0xc = 12
* 0xff = 255

# Example please

--- 

~~~~ {.c }
int main() {
    int a = 1045297778;
}
~~~~

~~~~~~
pwndbg> x &a
0x7fffffffdd8c:	0x3e4dfa72
~~~~~~~

Looks good.

---

~~~~ {.c }
int main() {
    int a = 1045297778;
}
~~~~

~~~~~~
pwndbg> x &a
0x7fffffffdd8c:	0x3e4dfa72
~~~~~~~

Looks good.

~~~~
pwndbg> x/b &a
0x7fffffffdd8c:	0x72
~~~~

What?

---

# Little-endian

* Numbers stored in reverse order
* First byte = least significant byte
* 0x11223344 = 0x44, 0x33, 0x22, 0x11

~~~
pwndbg> x/4xb &a
0x7fffffffdd8c:	0x72	0xfa	0x4d	0x3e
~~~

---

# Structs

---

~~~~ {.c}
struct MyStruct {
    int a;
    char b;
    int c;
    char d;
};
int main() {
    struct MyStruct my_struct;
    my_struct.a = 1;
    my_struct.b = 2;
    my_struct.c = 3;
    my_struct.d = 4;
}
~~~~

-----

Stack likes to be 4-byte aligned (why?)

Hence, "dead space"

~~~ {.c}
struct MyStruct my_struct;
my_struct.a = 1;
my_struct.b = 2;
my_struct.c = 3;
my_struct.d = 4;
~~~

Here, `sizeof MyStruct` is 16

~~~ {.c}
mov    DWORD PTR [rbp-0x10],0x1
mov    BYTE PTR [rbp-0xc],0x2
mov    DWORD PTR [rbp-0x8],0x3
mov    BYTE PTR [rbp-0x4],0x4
~~~


# Arrays

# What's an array

* A list of values
* All next to one another

~~~ {.c}
int myArray[2];
myArray[2] = 4;
~~~

Wait, is that too big of an index?

# We just overflowed

Whatever comes after myArray now?

# What else can flow?

---

~~~ {.c}
char myChar = 0;
while (1) {
    printf("%d\n", myChar);
    myChar ++;
}
~~~

---

```
0
1
2
...
126
127
-128
-127
...
-2
-1
0
1
2
```

# Notes for tonight

## GDB

* `set follow-fork-mode parent`
* Compile + run locally until solved
* Be creative!
