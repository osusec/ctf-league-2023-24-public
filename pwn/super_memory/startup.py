#!/usr/local/bin/python3

import os

while True:
    try:
        level = int(input("Please choose a level to play (1 or 2): "))
    except ValueError:
        print("Please enter a valid level number.")
        continue

    if level == 1:
        print("Starting up level 1 of Super Memory World...")
        os.execv("./level1", ["./level1"])

    elif level == 2:
        print("Starting up level 2 of Super Memory World...")
        os.execv("./level2", ["./level2"])

    else:
        print("No such level, please try again.")
