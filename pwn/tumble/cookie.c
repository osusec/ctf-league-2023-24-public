#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void get_flag(void) {
  // no using stack cause of base pointer overwrite
  puts("Flag:");
  puts(getenv("FLAG"));

  // prevent segfault or something lol
  exit(0);
}

void print_stack_frame(long first_local) {
  puts("====== STACK FRAME ======");
  puts("          top           |");
  puts("                        |");
  printf("  %#18lx    |\n", *(long *)(first_local + 24));
  printf("  %#18lx    |\n", *(long *)(first_local + 16));
  printf("  %#18lx    |\n", *(long *)(first_local + 8));
  printf("  %#18lx    |\n", *(long *)first_local);
  puts("                        |");
  puts("         bottom         v");
  puts("=========================");
}

int main(void) {
  setbuf(stdin, NULL);
  setbuf(stdout, NULL);
  setbuf(stderr, NULL);

  char buf[8] = {0};

  print_stack_frame((long)&buf);

  puts("\nAfter you overflowed your buffer on the last one, I did a bit of reading and learned about stack canaries! Including them prevents buffer overflows right??");
  printf("Input: ");

  gets(buf);

  puts("\nHere's the stack frame including your input:");
  print_stack_frame((long)&buf);

  return 0;
}
