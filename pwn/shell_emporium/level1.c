#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SHELLCOUNT 1
#define INSTRUCTION_LENGTH 255

int orderSeashell(char *specialInstructionsDestination, size_t specialInstructionsLength) {
    int userInput = -1;
    while (userInput < 1 || userInput > 4) {
        printf("Select a seashell from our sea source:\n");
        printf("(1) Soft shell\n");
        printf("(2) Smooth shell\n");
        printf("(3) Strong shell\n");
        printf("(4) Sure shell\n");
        printf("(1-4): ");
        scanf("%d", &userInput);
        for (char c = getchar();  c != '\n' && c != EOF; c = getchar());
        if (userInput < 1 || userInput > 4) {
            printf("We shall not sell the shell you seek.\n");
        }
    }
    printf("Please enter any special instructions for your order:\n");

    // char *buf = NULL;
    // size_t bufLen = 0;
    // getline(&buf, &bufLen, stdin);
    fgets(specialInstructionsDestination, 255, stdin);

    // strncpy(specialInstructionsDestination, buf, specialInstructionsLength);
    // free(buf);

    return userInput;
}

void ship() {
    char addr[8];
    printf("Sally's Seashell Emporium offers **shore-to-door** service\n");
    printf("Please enter your address, and we'll ship your shells to you!\n");
    fgets(addr, 32, stdin);
    return;
}

int main() {
    // disable stdin/stdout buffering
    setbuf(stdin, NULL);
    setbuf(stdout, NULL);

    char instructions[SHELLCOUNT][INSTRUCTION_LENGTH];
    printf("DEBUG MESSAGE, PLEASE IGNORE: %p\n", &instructions);
    printf("Welcome to Sally's Seashell Emporium!\n");
    for (int i = 0; i < SHELLCOUNT; i ++) {
        orderSeashell(instructions[i], INSTRUCTION_LENGTH);
    }

    ship();
}
