#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

char ticketlist[4096];

void read_ticketlist(void) {
  FILE *ticket_file = fopen("tickets.txt", "r");

  if (ticket_file == NULL) {
    strcpy(ticketlist, "But nobody came.");
  } else {
    fread(ticketlist, 1, sizeof(ticketlist), ticket_file);
    fclose(ticket_file);
  }
}

void read_flag(void) {
  char flag_buf[64] = {0};

  FILE *flag = fopen("flag.txt", "r");

  if (flag == NULL) {
    strcpy(flag_buf, "osu{n0w_7ry_0n_r3m073?}");
  } else {
    int nread = fread(flag_buf, 1, sizeof(flag_buf) - 1, flag);
    flag_buf[nread] = '\0';
    fclose(flag);
  }

  puts(flag_buf);
}

bool check_registered(void) {
  char name[32];

  // annoying hack around stack local ordering >:(
  volatile char admin_override[32];
  *(int *)admin_override = 1337;

  // I hate myself (need address cause %n is annoying)
  ((long long *)admin_override)[1] = &admin_override;

  printf("Name: ");
  fgets(name, sizeof(name), stdin);

  // replace trailing newline with null terminator
  int name_len = strlen(name);
  name[name_len - 1] = '\0';

  if (strlen(name) < 10) {
    puts("Sorry, the name you entered was too short. Please exit and try again.");
    exit(1);
  } else {
    printf("Searching for ");
    printf(name);
    printf(" in the ticket registry...\n");

    sleep(1);

    char *name_ptr = strstr(ticketlist, name);

    return name_ptr != NULL || *(int *)admin_override == 1;
  }
}

int main(void) {
  // disable IO buffering as always
  setbuf(stdin, NULL);
  setbuf(stdout, NULL);

  read_ticketlist();

  puts("Welcome to the TicketSecure™ Ticketing System™ (patent pending)!");
  puts("In order to verify you purchased a ticket for this event, please enter your information.");
  bool valid = check_registered();

  if (valid) {
    puts("Your ticket purchase record was found! Here's a flag as appreciation for your continued support of TicketSecure™:");
    read_flag();
    puts("Enjoy the event!");
  } else {
    puts("Sorry, no attendee with that name could be found. As such, we unfortunately can't let you into the event.");
  }

  return 0;
}
