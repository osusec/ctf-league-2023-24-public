#include <stdio.h>
#include <string.h>
#include <strings.h> // yes this is separate
#include <stdlib.h>

void print_file(char *filename, char *def) {
  char contents[65535];

  FILE *f = fopen(filename, "r");

  if (f == NULL) {
    strcpy(contents, def);
  } else {
    int nread = fread(contents, 1, sizeof(contents) - 1, f);
    contents[nread] = '\0';
    fclose(f);
  }

  puts(contents);
}

void read_flag(void) {
  char flag[64];

  FILE *f = fopen("flag.txt", "r");

  if (f == NULL) {
    strcpy(flag, "osu{n0w_7ry_0n_r3m073?}");
  } else {
    int nread = fread(flag, 1, sizeof(flag) - 1, f);
    flag[nread] = '\0';

    // I literally can't fclose f without segfaulting so idc
  }

  puts(flag);
}

void get_signature(void) {
  char buf[32] = {0};
  int len = 0;

  printf("Do you agree to the terms and conditions? (y/n) ");
  fgets(buf, sizeof(buf), stdin);

  // replace trailing newline with null terminator
  len = strlen(buf);
  buf[len - 1] = '\0';

  if (strcasecmp("y", buf) != 0) {
    puts("As you do not agree to the terms and conditions, you cannot use the TicketSecure™ platform.");
    exit(1);
  } else {
    printf("Enter your name: ");

    fgets(buf, sizeof(buf), stdin);
    len = strlen(buf);
    buf[len - 1] = '\0';

    printf("Name: ");
    printf(buf);
    putchar('\n');

    printf("Today's date: ");
    fgets(buf, sizeof(buf)*2, stdin);

    puts("Thank you for your signature");
  }
}

int main(void) {
  // disable stdin/stdout buffering
  setbuf(stdin, NULL);
  setbuf(stdout, NULL);

  print_file("agreement.txt", "Agreement not found.");

  get_signature();
  
  return 0;
}
