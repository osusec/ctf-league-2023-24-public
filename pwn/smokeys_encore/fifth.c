#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>


void read_flag(){
    printf("%s", getenv("FLAG"));
}

int main()
{
    setbuf(stdout, NULL);
    setbuf(stdin, NULL);

    volatile unsigned long long other_check = 0;
    volatile int super_check = 0;
    volatile int constant = 0;
    char information[100];

    char credit_card[21];
    memset(&credit_card, 0, 21);

    printf("I am now satisfied in knowing you are a robot.\n");
    printf("And to ensure you don't pwn me again, I've added more defenses and checks! If you still manage it, somehow, then I'll reward you.\n");


    usleep(1000*700);

    printf("Beep boop\n");

    usleep(1000*700);


    printf("Now, I will open up my functionality to you. As I said, I am SMOKEY\n");

    usleep(400*1000);
    printf("I am a computerized system to help you with your every need\n");
    printf("\n");


    printf("1. Entertainment.\n");
    printf("2. Exercise.\n");
    printf("3. Store.\n");
    printf("4. Give flag.\n");
    printf("5. Quit.\n");

    printf("Pick an option: ");
    int option;
    scanf("%d", &option);

    switch(option){
        case 1:{
            printf("%s\n", "https://www.youtube.com/watch?v=dQw4w9WgXcQ");
            break;
        }
        case 2:{
            printf("What kind of robot needs exercise? Maybe run some unit tests to make sure you are working correctly\n");
            break;
        }
        case 3:{

            printf("Need to buy something? Sure! What can I do for ya?\n");

            printf("1. Buy more RAM\n");
            printf("2. Buy faster internet\n");
            printf("3. New keyboard\n");
            printf("4. New operating system\n");
            printf("5. My hard disk drive is not spinning fast enough!\n");

            int store;
            scanf("%d",&store);
            
            switch(store){
                case 1: {
                    void(*CRASH)() = NULL;
                    (CRASH)();
                    break;
                }

                case 2:{
                    printf("Sure can do! I am sending over the bytes to make your internet 100x faster!\n");
                    printf("First, I'll need your credit card and social security numbers - to verify your digital identity, of course!\n");

                    printf("Credit card number (XXXX-XXXX-XXXX-XXXX): ");
                    scanf("%20s", &credit_card);

                    printf("And your social security number (XXX-XX-XXX)?");
                    read(0,&information,0x100);
                
                    if(constant == 1 && super_check == 0x1337 && other_check == 0xDEADBEEF){
                        printf("\n");
                        printf("Well done - the pwn gods look down approvingly.\n");
                        read_flag();
                    } else {
                        printf("\n");
                        printf("SIKE! MUAHAHAHA I can now steal your identity! Sucka!!");
                        printf("\n");
                    }
                    
                    break;
                }

                case 3: {
                    printf("Klick klack!\n");
                    break;
                }

                case 4:{
                    printf("Have you tried Linux? It's free - don't need to buy it!\n");
                    break;
                }

                case 5: {
                    printf("%s", "Hmm... is it spinning like this - https://www.youtube.com/watch?v=XxObcVn1DMQ? If so, there's nothing we can do\n");
                    break;
                }
            }
          
            break;
        }
        case 4: {
            printf("Sure! I'll give you my flag - just one moment, looking for it in my memory\n");
            sleep(2);
            printf("\n");
            printf("Hmm. It sure is disorganized in here\n");
            sleep(3);
            printf("\n");
            printf("Could have sworn I put it right under my big pile of cookies...\n\n");

            printf("");
            sleep(4);
            printf(".");
            sleep(4);
            printf(".");
            sleep(1);
            printf(".");
            sleep(4);
            printf(".");
            sleep(2);
            printf(".");
            sleep(7);
            printf(".\n\n");


            sleep(5);
            printf("Oh, you know what, I left it in my car! I'll be right back!\n\n");

            sleep(4);
            printf("'You hear the sound of SMOKEY's computerized footsteps as opens and closes the door and runs to the car'\n\n");
            
            sleep(9);

            printf("'You hear the car door open and close'\n\n");
            
            sleep(4);

            printf("'You hear the skidding of wheels as SMOKEY takes off!'\n\n");

            sleep(2);


            printf("'Guess you'll have to get the flag another way!'\n");



            break;
        }
        case 5:{
            printf("Cya l8r!\n");
            break;
        }
    }

}


