#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

void read_flag(){
    printf("%s", getenv("FLAG"));
}

int main()
{
    setbuf(stdout, NULL);
    setbuf(stdin, NULL);

    volatile int constant = 0;
    char name[50];
    memset(&name, 0, 50);

    printf("I don't think I ever got your name? What is it?!\n");

    read(0, &name, 0x50);

    printf("Hello, %s\n",&name);

    if(constant != 0){
        printf("Woah! How did you get here? Did you just pwn me?!?$^@#?\n");
        read_flag();
    }
}


