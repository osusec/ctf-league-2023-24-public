from z3 import*

"""
Consider the following puzzle. Spend exactly 100 dollars and buy exactly 100 animals. 
Dogs cost 15 dollars, cats cost 1 dollar, and mice cost 25 cents each. 
You have to buy at least one of each. How many of each should you buy?
"""

def solve_constraint(cost):
    Taffy, Chocolate, Gumballs, Jawbreakers = Ints('Taffy Chocolate Gumballs Jawbreakers')

    s = Solver()


    s.add(Taffy + Chocolate + Gumballs + Jawbreakers == 100)
    s.add(ToReal(Taffy)*0.75 + ToReal(Chocolate)*1.5 + ToReal(Gumballs)*3.75 + ToReal(Jawbreakers)*4 == cost)
    s.add(Taffy >= 0)
    s.add(Chocolate >= 0)
    s.add(Gumballs >= 0)
    s.add(Jawbreakers >= 0)
    if(s.check() == sat):
        m = s.model()
        print(m[Taffy], m[Chocolate], m[Gumballs], m[Jawbreakers])
        return cost
    else :
        print("unsolvable")
        return 0



#INFO: Used to check for possible solutions
#possible_values = []
#for x in range(100, 1000):
    #cost = solve_constraint(x)
    #if(cost):
        #possible_values.append(cost)


#print(possible_values)


cost = input("Input: ")
solve_constraint(cost)
