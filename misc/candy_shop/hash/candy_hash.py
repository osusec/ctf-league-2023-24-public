#!/usr/local/bin/python3
from random import randrange
import select, sys
import math
from secrets import flag, passwords

candy_constant = 311

def print_flag():
    print(flag)


def candy_hash(s):
    n = 0
    for c in s:
        n *= candy_constant
        n += ord(c)
    return n

def the_deal():
    index = randrange(len(passwords))
    password = passwords[index]
    print("You pull at the note...noticing too late that it was attached to a wire!");
    print("You hear a click as the pad states you have five minutes to enter the correct code")
    print("You see on the note, \"14 characters\" and you flip it over and see:") 
    print(candy_hash(password))
    answer = input("You reach for the pad, what do you enter? ") 
    if(answer == password):
        print("The door swings open and you gourge yourself on the candy within")
        print("...Oh and you also find the flag")
        print_flag()
        return 1;
        
    else:
        print("The alarms start blaring as you run away");


def setting_print():
    print("You hunger for more candy, sneaking to the candy shop you see that it is locked with a password.")
    print("On the door you see a post it note.")
    print("1: Look at the note");
    print("2: Walk back still hungering for candy");
    choice = input();
    if(choice == "1"):
        if(the_deal()):
            return
    print("Your stomach growls from the lack of sugar");
    return;




setting_print();
