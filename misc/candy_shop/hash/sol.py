#!/usr/bin/env python3
from z3 import *
candy_constant = 311

def candy_hash(s):
    n = 0
    for c in s:
        n *= candy_constant
        n += ord(c)
    return n


def z3_candy_hash(s):
    m = 0
    for c in s:
        m *= candy_constant # orig: 31
        m += c
    return m

# N is length of password, usually want 1 num_sols, but useful for finding collissions
def crack_password(hash_value,n,num_sols=0):
    print(hash_value)
    p = [z3.Int(f"p_{i}") for i in range(n)]
    s = Solver()
    for i in range(n):
        # original range
        # s.add(And(p[i] >= ord('a'), p[i] <= ord('z')))
        # A better range " " .. "~"
        s.add(And(p[i] >= 32, p[i] <= 126))

    s.add(z3_candy_hash(p) == hash_value)

    passwords = []
    if s.check() == sat:
        found_sols = 0
        while s.check() == sat:
            mod = s.model()
            # print([mod[t].as_long() for t in p])
            password = "".join([chr(mod[t].as_long()) for t in p])
            print("password:",password, flush=True)
            passwords.append(password)
            if num_sols > 0 and found_sols < num_sols:
                break
            getDifferentSolution(s,mod, p)
            found_sols += 1
        return passwords
    else:
        return None
#INFO: Used to test passwords were findable
#passwords = ["candyiscool123", "WouldRatherEat", "SweetAsCandy12", "ChocolateChip1", "CottonCandyDaz", "RedVinesTwist63", "MilkyWayMagic3", "TootsieRollTim", "SourPatchKids9"]
    #for x in passwords:
        #crack_password(candy_hash(x), 14, 1)
hash = input("Input Hash: ")
print(crack_password(hash, 14, 1))
