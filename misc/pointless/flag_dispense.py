#!/usr/local/bin/python3

import json
import os
import secrets

from web3 import Web3

# Generate random token per invocation for the user
token = secrets.token_bytes(32)
print("Welcome to Pointless, where there's no use in trying at all!")
print(f"Your token is gonna be 0x{token.hex()}")
print(
    "Come back in 10 days or whatever when you get enough points and then maybe I'll give you the flag :)"
)
input("Press enter when you're ready: ")

# Yoinked from remix (solc is annoying to deal with)
POINTLESS_ABI = [
    {
        "anonymous": False,
        "inputs": [
            {
                "indexed": False,
                "internalType": "bytes32",
                "name": "token",
                "type": "bytes32",
            }
        ],
        "name": "StreakReward",
        "type": "event",
    },
    {
        "inputs": [],
        "name": "claimDaily",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function",
    },
    {
        "inputs": [{"internalType": "bytes32", "name": "token", "type": "bytes32"}],
        "name": "claimStreakReward",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function",
    },
]

pointless_abi_str = json.dumps(POINTLESS_ABI, separators=(",", ":"))

# Main RPC endpoint was down for some reason so we use a secondary one
w3 = Web3(Web3.HTTPProvider("https://rpc.sepolia.org/"))

# Environment config yAYa
address = os.getenv("POINTLESS_ADDRESS")
creation_block = int(os.getenv("POINTLESS_CREATE_BLOCK"))

# Actually connect (?) to the contract
pointless = w3.eth.contract(address=address, abi=pointless_abi_str)

# Check if proper event emitted with the given token
logs = pointless.events.StreakReward().get_logs(
    fromBlock=creation_block, argument_filters={"token": token}
)

# If logs is a nonempty list, they solved the challenge
if logs:
    print(
        "Oh, you actually did it? I wasn't expecting that, here's your pointless flag though I guess:"
    )
    with open("flag.txt") as flag:
        print(flag.read())

# ascii shrug ascii shrug
else:
    print(
        "Nice try, but you don't have enough points for the flag. Better luck next time I guess ¯\\_(ツ)_/¯"
    )
