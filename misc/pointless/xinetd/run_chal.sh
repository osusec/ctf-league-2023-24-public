#!/bin/sh

# wow what do you know more DamCTF 2023 stuff :)

# no stderr
exec 2>/dev/null

# dir
cd /chal

# timeout after 120 sec
timeout -k1 120 stdbuf -i0 -o0 -e0 ./flag_dispense.py
#           |^  |^^^^^^^^^^^^^^^^^
#           |   + disable buffering
#           + 120s timeout

