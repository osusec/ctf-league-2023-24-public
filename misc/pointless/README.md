# misc/pointless - baby web3 chal

Challenge boils down to contract reentrancy on the `Pointless` contract to get more than the allotted 10 points per day in...a single day.
Players will have to submit a token as part of the streak claim thing so that the server (not the contract) knows they got the challenge.

My solve is located at [`Hearmeout.sol`](./Hearmeout.sol).
