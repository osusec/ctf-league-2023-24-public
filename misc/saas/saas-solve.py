from pwn import *

import string
import time

# basically string.printable without whitespace characters
chars = string.ascii_letters + string.digits + string.punctuation

if __name__ == "__main__":
    # context.log_level = "debug"

    io = remote("localhost", 1320)
    io.recvuntil(b"demo!\n")

    per_chars = []
    for _ in range(5):
        before = time.perf_counter_ns()
        io.sendlineafter(b"flag: ", b"b")
        io.recvuntil(b"again :)")
        per_chars.append(time.perf_counter_ns() - before)

    print(per_chars)

    # we know the first character is an o
    o_durations = []

    for _ in range(5):
        before_o = time.perf_counter_ns()
        io.sendlineafter(b"flag: ", b"o")
        io.recvuntil(b"again :)")
        o_durations.append(time.perf_counter_ns() - before_o)

    print(o_durations)

    try:
        # flag can't be empty cause indexing yay
        print(chars)
        flag = "osu{"
        while flag[-1] != "}":
            pre_char = time.perf_counter_ns()
            flag_enc = flag.encode()

            durations = []
            for c in chars:
                before = time.perf_counter_ns()
                io.sendlineafter(b"flag: ", flag_enc + c.encode())
                io.recvline()
                durations.append(time.perf_counter_ns() - before)
        
            print(durations)
            max_dur = min(durations)
            print(max_dur)
            idx = durations.index(max_dur)
            print("char:", chars[idx])
            print("dur:", time.perf_counter_ns() - pre_char)
            flag += chars[idx]
            print("flag:", flag)
            print()
    except EOFError:
        print("full flag:", flag + "}")
