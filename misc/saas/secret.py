import time

FLAG = "osu{w4171n9_f0r_muh_fl49}"

def proprietary_expensive_comparison_function_with_super_long_name_that_you_should_probably_pay_attention_to(char1, char2):
    equal = char1 == char2

    if not equal:
        time.sleep(0.007)

    return equal
