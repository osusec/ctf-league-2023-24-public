#!/usr/local/bin/python3

from itertools import zip_longest
from secret import FLAG, proprietary_expensive_comparison_function_with_super_long_name_that_you_should_probably_pay_attention_to

print("Welcome to the Pointless strcmp-as-a-service (SaaS)℠ demo!")


while True:
    comparison_str = input("Please enter a string to compare to our secret flag: ")

    equal = True
    # for c1, c2 in zip_longest(comparison_str, FLAG, fillvalue="\x00"):
    for c1, c2 in zip(comparison_str, FLAG):
        equal = equal and proprietary_expensive_comparison_function_with_super_long_name_that_you_should_probably_pay_attention_to(c1, c2)
        if not equal:
            break

    if equal and len(comparison_str) == len(FLAG):
        print("You've got it!")
        break
    else:
        print("Not quite! We'll let you try again :)")
