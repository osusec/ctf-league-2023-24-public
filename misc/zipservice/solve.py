
# Modified ChatGPT Solver
# It does work
# CRC corrected with reference: https://stackoverflow.com/questions/69061089/zips-crc-32-for-encryption-isnt-quite-zlibs-crc32-why
import zlib

def crc32(ch, crc):
    return ~zlib.crc32(bytes([ch]), ~crc) & 0xffffffff

def initialize_keys(password):
    keys = [305419896, 591751049, 878082192]
    for char in password:
        keys = update_keys(keys, char)
    return keys

def update_keys(keys, char):
    keys[0] = crc32(char, keys[0]) & 0xFFFFFFFF
    keys[1] = (keys[1] + (keys[0] & 0x000000FF)) & 0xFFFFFFFF
    keys[1] = (keys[1] * 134775813 + 1) & 0xFFFFFFFF
    keys[2] = crc32((keys[1] >> 24), keys[2]) & 0xFFFFFFFF
    return keys

def decrypt_byte(keys):
    temp = (keys[2] | 2) & 0xFFFF
    return ((temp * (temp ^ 1)) >> 8) & 0xFF

def decrypt_header(buffer, keys):
    for i in range(12):
        c = buffer[i] ^ decrypt_byte(keys)
        keys = update_keys(keys, c)
        buffer[i] = c
    print(buffer)

# Usage
password = b"Test"
keys = initialize_keys(password)

# Assuming `encrypted_header` is the 12-byte header you want to decrypt
encrypted_header = bytearray(b'\xd0\xb0\xd4\x9f\xb4\xe1\xd3\x0d\x24\xd7\x83\x48')
decrypt_header(encrypted_header, keys)
