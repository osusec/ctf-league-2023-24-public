zipservice 

"Zip As A Service"

Functionality:
- Takes b64(size limited) and text input from user
- Creates encrypted non-compressed zipfile with b64 as data and text as password
Using "Traditional PKWARE Encryption", a symmetric stream cypher
- Under normal operation a random 11 (+ 1 byte check char) byte "Encryption Header" is used between the password and data
- It is encrypted and stored along-side the data
- Here that header will be the flag (osu{"6 byte flag"})

The enryption header starts at 0x33 after a section of null bytes. After being initialised with the password, the next 11 bytes are the encrypted flag. Doing this requires implementation of the algorithm, which may be hard for those with less programming experience.

"(Copyed from the APPNOTE)
Step 1 - Initializing the encryption keys
-----------------------------------------

Key(0) <- 305419896
Key(1) <- 591751049
Key(2) <- 878082192

loop for i <- 0 to length(password)-1
    update_keys(password(i))
end loop

Where update_keys() is defined as:

update_keys(char):
  Key(0) <- crc32(key(0),char)
  Key(1) <- Key(1) + (Key(0) & 000000ffH)
  Key(1) <- Key(1) * 134775813 + 1
  Key(2) <- crc32(key(2),key(1) >> 24)
end update_keys

Where crc32(old_crc,char) is a routine that given a CRC value and a
character, returns an updated CRC value after applying the CRC-32
algorithm described elsewhere in this document.

Step 2 - Decrypting the encryption header
-----------------------------------------

The purpose of this step is to further initialize the encryption
keys, based on random data, to render a plaintext attack on the
data ineffective.

Read the 12-byte encryption header into Buffer, in locations
Buffer(0) thru Buffer(11).

loop for i <- 0 to 11
    C <- buffer(i) ^ decrypt_byte()
    update_keys(C)
    buffer(i) <- C
end loop

Where decrypt_byte() is defined as:

unsigned char decrypt_byte()
    local unsigned short temp
    temp <- Key(2) | 2
    decrypt_byte <- (temp * (temp ^ 1)) >> 8
end decrypt_byte
"
