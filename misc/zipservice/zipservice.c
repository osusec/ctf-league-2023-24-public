#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#define KEY0 305419896
#define KEY1 591751049
#define KEY2 878082192


char ValidB64Chars[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '=', '/' ,'+'};

uint32_t crc32(uint32_t old_crc, char c) {
    static uint32_t crc_table[256];
    if (!crc_table[1]) {
        for (int i = 0; i < 256; i++) {
            uint32_t crc = i;
            for (int j = 0; j < 8; j++) {
                crc = (crc >> 1) ^ ((crc & 1) ? 0xEDB88320 : 0);
            }
            crc_table[i] = crc;
        }
    }
    return (old_crc >> 8) ^ crc_table[(old_crc ^ c) & 0xFF];
}

void update_keys(char c, uint32_t *key0, uint32_t *key1, uint32_t *key2) {
    *key0 = crc32(*key0, c);
    *key1 += (*key0 & 0x000000FF);
    *key1 = (*key1 * 134775813) + 1;
    *key2 = crc32(*key2, *key1 >> 24);
}

unsigned char decrypt_byte(uint32_t key2) {
    unsigned short temp = key2 | 2;
    return (temp * (temp ^ 1)) >> 8;
}

int main()
	{
	setbuf(stdin, NULL);
	setbuf(stdout, NULL);
	char Flag[12];
	FILE *FlagFile = fopen("flag", "r");
	if (FlagFile)
	{
		fread(Flag, 1, 11, FlagFile);
		fclose(FlagFile);
	}
	else
	{
		strcpy(Flag, "osu{NOFLAG}");
	}
	
	printf("Welcome to ZIP as a Service!\nProviding all your archive needs since 2023.\n");
	printf("Send your data as base64: ");
	char B64Data[130];
	fgets(B64Data, 130, stdin);
  	if (strlen(B64Data)<2){
		printf("Error: no data");
		return 0;
	}
	B64Data[strlen(B64Data) - 1] = 0;
	for (char *Chck = B64Data; *Chck; Chck++) {
		if (strchr(ValidB64Chars, *Chck) == NULL)
		{
			printf("Error: invalid char");
			return 0;
		}
	}
	
	printf("Enter your desired password: ");
	char Password[16];
	fgets(Password, 16, stdin);
	if (strlen(Password)<2){
		printf("Error: no password");
		return 0;
	}
	Password[strlen(Password) - 1] = 0;
	for (char *Chck = Password; *Chck; Chck++) {
		if (strchr(ValidB64Chars, *Chck) == NULL)
		{
			printf("Error: invalid char");
			return 0;
		}
	}
	
	char ZipFileName[L_tmpnam];
	tmpnam(ZipFileName);
	char Command[256];
	sprintf(Command, "echo \"%s\" | base64 -d | zip -0q -P %s > %s", B64Data, Password, ZipFileName);
	system(Command);

	// Using code from chat GPT
    uint32_t key0 = KEY0;
    uint32_t key1 = KEY1;
    uint32_t key2 = KEY2;

    for (int i = 0; Password[i]; i++) {
        update_keys(Password[i], &key0, &key1, &key2);
    }
	
	FILE *ZipFile = fopen(ZipFileName, "r+b");
	fseek(ZipFile, 0x33, SEEK_SET);
    char Header[12];
	fread(Header, 1, 12, ZipFile);
	
    for (int i = 0; i < 12; i++) {
        char c = Header[i] ^ decrypt_byte(key2);
        update_keys(c, &key0, &key1, &key2);
        Header[i] = c;
    }
	uint8_t ChckByte = Header[11];


	key0 = KEY0;
    key1 = KEY1;
    key2 = KEY2;

	// Password
    for (int i = 0; Password[i]; i++) {
        update_keys(Password[i], &key0, &key1, &key2);
    }
	// Header
	for (int i = 0; i < 11; i++) {
        Header[i] = Flag[i] ^ decrypt_byte(key2);
        update_keys(Flag[i], &key0, &key1, &key2);
    }
	// CheckByte
	Header[11] = ChckByte ^ decrypt_byte(key2);
	update_keys(ChckByte, &key0, &key1, &key2);
	
	fseek(ZipFile, 0x33, SEEK_SET);
	fwrite(Header,12,1,ZipFile);

	char Command2[256];
	sprintf(Command2, "echo \"%s\" | base64 -d", B64Data);
	FILE* InDataStream = popen(Command2, "r");

	char InDataChar;
	char OutDataChar;
	// Data
	for (int i = 0; i < 130; i++) {
		char InDataChar = fgetc(InDataStream); 
        if (InDataChar == EOF) {
            break; 
        }
        OutDataChar = InDataChar ^ decrypt_byte(key2);
        update_keys(InDataChar, &key0, &key1, &key2);
		fwrite(&OutDataChar, 1, 1, ZipFile);
    }
	

	fclose(ZipFile);
	
	char Command3[256];
	sprintf(Command3, "base64 %s", ZipFileName);
	FILE* FinalData = popen(Command3, "r");
	
	char c;
	while ((c = fgetc(FinalData)) != EOF) {
        putchar(c);
    }

	pclose(InDataStream);
	pclose(FinalData);
	remove(ZipFileName);
    return 0;
}